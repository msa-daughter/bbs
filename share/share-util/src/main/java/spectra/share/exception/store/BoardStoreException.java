package spectra.share.exception.store;

import spectra.share.exception.BoardException;

public class BoardStoreException extends BoardException {
    //
    public BoardStoreException(String message) {
        super(message);
    }

    public BoardStoreException(String message, Throwable t) {
        super(message, t);
    }

    public BoardStoreException(Throwable t) {
        super(t);
    }
}
