package spectra.share.exception.store;

public class NonExistenceException extends BoardStoreException {
    //
    public NonExistenceException(String message) {
        super(message);
    }

    public NonExistenceException(String message, Throwable t) {
        super(message, t);
    }

    public NonExistenceException(Throwable t) {
        super(t);
    }
}
