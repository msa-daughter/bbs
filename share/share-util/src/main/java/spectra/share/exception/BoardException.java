package spectra.share.exception;

public class BoardException extends RuntimeException {
    //
    public BoardException(String message) {
        super(message);
    }

    public BoardException(String message, Throwable t) {
        super(message, t);
    }

    public BoardException(Throwable t) {
        super(t);
    }
}
