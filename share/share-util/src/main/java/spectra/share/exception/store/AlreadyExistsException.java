package spectra.share.exception.store;

public class AlreadyExistsException extends BoardStoreException {
    //
    public AlreadyExistsException(String message) {
        super(message);
    }

    public AlreadyExistsException(String message, Throwable t) {
        super(message, t);
    }

    public AlreadyExistsException(Throwable t) {
        super(t);
    }

}
