package spectra.share.util.date;

import java.time.ZonedDateTime;
import java.time.format.DateTimeFormatter;

public class DateUtil {
    //
    public static String now() {
        return DateTimeFormatter.ofPattern("yyyyMMddhhmmss").format(ZonedDateTime.now());
    }
}
