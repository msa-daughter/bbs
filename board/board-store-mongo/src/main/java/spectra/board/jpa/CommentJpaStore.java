package spectra.board.jpa;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import spectra.board.domain.entity.Comment;
import spectra.board.domain.store.CommentStore;
import spectra.board.jpa.jpo.CommentJpo;
import spectra.board.jpa.springdata.CommentRepository;
import spectra.share.exception.store.NonExistenceException;

import java.util.List;
import java.util.NoSuchElementException;

@Repository
public class CommentJpaStore implements CommentStore {
    //
    @Autowired
    private CommentRepository commentRepository;

    @Override
    public String create(Comment comment) {
        CommentJpo commentJpo = CommentJpo.toJpo(comment);
        commentRepository.save(commentJpo);
        return comment.getId();
    }

    @Override
    public Comment retrieve(String id) throws NoSuchElementException {
        CommentJpo commentJpo =  commentRepository.findOne(id);
        if (commentJpo == null)
            throw new NoSuchElementException(String.format("No comment jpo[ID:%s] to retrieve.", id));

        return commentJpo.toDomain();
    }

    @Override
    public List<Comment> retrieveByPostId(String postId) {
        List<CommentJpo> commentJpos =  commentRepository.findByPostId(postId);
        return commentJpos == null ? null : CommentJpo.toDomains(commentJpos);
    }

    @Override
    public void update(Comment comment) {
        if (!commentRepository.exists(comment.getId()))
            throw new NonExistenceException(String.format("No comment document[ID:%s] to update.", comment.getId()));

        CommentJpo commentJpo = CommentJpo.toJpo(comment);
        commentRepository.save(commentJpo);
    }

    @Override
    public void delete(Comment comment) {
        commentRepository.delete(comment.getId());
    }

    @Override
    public void delete(String id) {
        commentRepository.delete(id);
    }
}
