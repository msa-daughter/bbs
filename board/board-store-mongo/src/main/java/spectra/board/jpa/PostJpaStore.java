package spectra.board.jpa;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import spectra.board.domain.entity.Post;
import spectra.board.domain.store.PostStore;
import spectra.board.jpa.jpo.PostJpo;
import spectra.board.jpa.springdata.PostRepository;
import spectra.share.exception.store.AlreadyExistsException;
import spectra.share.exception.store.NonExistenceException;
import spectra.share.springdata.page.OffsetRequest;

import java.util.List;
import java.util.NoSuchElementException;

@Repository
public class PostJpaStore implements PostStore {

    @Autowired
    private PostRepository postRepository;

    @Override
    public String create(Post post) {

        String id = post.getId();

        if (postRepository.exists(id)) throw new AlreadyExistsException(String.format("Post jpo[ID:%s] already exist.", id));

        PostJpo postJpo = PostJpo.toJpo(post);
        postRepository.save(postJpo);

        return id;
    }

    @Override
    public Post retrieve(String id) throws NoSuchElementException {
        PostJpo postJpo = postRepository.findOne(id);
        if(postJpo == null) {
            throw new NoSuchElementException(String.format("Post jpo[ID:%s] no element", id));
        }

        return postJpo.toDomain();
    }

    @Override
    public List<Post> retrieveByBlogId(String blogId, int offset, int limit){
        List<PostJpo> postJpos = postRepository.findByBlogId(blogId, new OffsetRequest(offset, limit));

        return PostJpo.toDomains(postJpos);
    }

    @Override
    public List<Post> retrieveByCategoryId(String categoryId, int offset, int limit){
        List<PostJpo> postJpos = postRepository.findByCategoryId(categoryId, new OffsetRequest(offset, limit));

        return PostJpo.toDomains(postJpos);
    }

    @Override
    public List<Post> retrieveByContent(String content, int offset, int limit) {

        List<PostJpo> postJpos = postRepository.findByContent(content, new OffsetRequest(offset, limit));
        return PostJpo.toDomains(postJpos);
    }

    @Override
    public List<Post> retrieveByTitle(String title, int offset, int limit) {
        List<PostJpo> postJpos = postRepository.findByTitle(title, new OffsetRequest(offset, limit));
        return PostJpo.toDomains(postJpos);
    }

    @Override
    public List<Post> retrieveByTitleAndContent(String title, String content, int offset, int limit) {
        List<PostJpo> postJpos = postRepository.findByTitleAndContent(title, content, new OffsetRequest(offset, limit));
        return PostJpo.toDomains(postJpos);
    }

    @Override
    public void update(Post post) {
        if (!postRepository.exists(post.getId()))
            throw new NonExistenceException(String.format("No post document[ID:%s] to update.", post.getId()));
        PostJpo citizenJpo = PostJpo.toJpo(post);
        postRepository.save(citizenJpo);
    }

    @Override
    public void delete(Post post) {
        delete(post.getId());
    }

    @Override
    public void delete(String id) {
        postRepository.delete(id);
    }
}
