package spectra.board.jpa;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import spectra.board.domain.entity.Tag;
import spectra.board.domain.store.TagStore;
import spectra.board.jpa.jpo.TagJpo;
import spectra.board.jpa.springdata.TagRepository;
import spectra.share.exception.store.NonExistenceException;
import spectra.share.springdata.page.OffsetRequest;
import spectra.share.exception.store.AlreadyExistsException;

import java.util.List;
import java.util.NoSuchElementException;

@Repository
public class TagJpaStore implements TagStore {

    @Autowired
    private TagRepository tagRepository;

    @Override
    public String create(Tag tag) {
        String id = tag.getId();
        if(tagRepository.exists(id)) throw new AlreadyExistsException(String.format("Tag jpo[ID:%s] already exist.", id));

        TagJpo tagJpo = TagJpo.toJpo(tag);
        tagRepository.save(tagJpo);
        return id;
    }

    @Override
    public Tag retrieve(String id) throws NoSuchElementException {
        TagJpo tagJpo = tagRepository.findById(id);
        if (tagJpo  == null) throw new NoSuchElementException(String.format("Post jpo[ID:%s] no element", id));
        return tagJpo.toDomain();
    }

    @Override
    public Tag retrieveByTagName(String tagName) {
        TagJpo tagJpo = tagRepository.findByTagName(tagName);
        Tag tag = tagJpo == null? null : tagJpo.toDomain();
        return tag;
    }

    @Override
    public List<Tag> retrieveByBlogId(String blogId, int offset, int limit) {
        List<TagJpo> tagJpos = tagRepository.findByBlogId(blogId, new OffsetRequest(offset, limit));
        return TagJpo.toDomains(tagJpos);
    }

    @Override
    public void update(Tag tag) {
        if (!tagRepository.exists(tag.getId()))
            throw new NonExistenceException(String.format("No tag document[ID:%s] to update.", tag.getId()));
        TagJpo citizenJpo = TagJpo.toJpo(tag);
        tagRepository.save(citizenJpo);
    }
}
