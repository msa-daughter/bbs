package spectra.board.jpa.springdata;

import org.springframework.data.domain.Pageable;
import org.springframework.data.mongodb.repository.MongoRepository;
import spectra.board.jpa.jpo.TagJpo;

import java.util.List;

public interface TagRepository extends MongoRepository<TagJpo, String> {
    TagJpo findById(String postId);
    TagJpo findByTagName(String tagName);
    List<TagJpo> findByBlogId(String blogId, Pageable pageable);
}
