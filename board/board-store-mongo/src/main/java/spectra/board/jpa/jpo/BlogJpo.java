package spectra.board.jpa.jpo;

import spectra.board.domain.entity.Blog;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import java.util.List;
import java.util.stream.Collectors;

@Entity(name = "T_BLOG")
public class BlogJpo {
    @Id
    private String id;

    private String blogName;
    private String description;
    @Column(unique = true)
    private String userId;
    private String userName;
    private String createdDate;

    private Long entityVersion;

    public BlogJpo() {
        //
    }

    public static BlogJpo toJpo(Blog blog) {
        BlogJpo blogJpo = new BlogJpo();
        blogJpo.setId(blog.getId());
        blogJpo.setBlogName(blog.getBlogName());
        blogJpo.setDescription(blog.getDescription());
        blogJpo.setUserId(blog.getUserId());
        blogJpo.setUserName(blog.getUserName());
        blogJpo.setCreatedDate(blog.getCreatedDate());
        blogJpo.setEntityVersion(blog.getEntityVersion());
        return blogJpo;
    }

    public Blog toDomain() {
        Blog blog = new Blog(id);
        blog.setBlogName(blogName);
        blog.setDescription(description);
        blog.setUserId(userId);
        blog.setUserName(userName);
        blog.setCreatedDate(createdDate);
        blog.setEntityVersion(entityVersion);
        return blog;
    }

    public static List<Blog> toDomains(List<BlogJpo> blogJpos){
        return blogJpos.stream()
                .map(jpo->jpo.toDomain())
                .collect(Collectors.toList());
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getBlogName() {
        return blogName;
    }

    public void setBlogName(String blogName) {
        this.blogName = blogName;
    }

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getCreatedDate() {
        return createdDate;
    }

    public void setCreatedDate(String createdDate) {
        this.createdDate = createdDate;
    }

    public Long getEntityVersion() {
        return entityVersion;
    }

    public void setEntityVersion(Long entityVersion) {
        this.entityVersion = entityVersion;
    }

}
