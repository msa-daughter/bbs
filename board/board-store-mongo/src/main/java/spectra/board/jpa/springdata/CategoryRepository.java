package spectra.board.jpa.springdata;

import org.springframework.data.mongodb.repository.MongoRepository;
import spectra.board.jpa.jpo.CategoryJpo;

import java.util.List;

public interface CategoryRepository extends MongoRepository<CategoryJpo, String> {
    CategoryJpo findById(String id);
    List<CategoryJpo> findByBlogId(String blogId);
}
