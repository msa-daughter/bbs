package spectra.board.jpa.springdata;

import org.springframework.data.domain.Pageable;
import org.springframework.data.mongodb.repository.MongoRepository;
import spectra.board.jpa.jpo.PostJpo;

import java.util.List;

public interface PostRepository extends MongoRepository<PostJpo, String> {
    PostJpo findById(String postId);
    List<PostJpo> findByBlogId(String blogId, Pageable pageable);
    List<PostJpo> findByCategoryId(String categoryId, Pageable pageable);
    List<PostJpo> findByContent(String content, Pageable pageable);
    List<PostJpo> findByTitle(String title, Pageable pageable);
    List<PostJpo> findByTitleAndContent(String title, String content,  Pageable pageable);

}
