package spectra.board.jpa.springdata;

import org.springframework.data.mongodb.repository.MongoRepository;
import spectra.board.jpa.jpo.BlogJpo;

public interface BlogRepository extends MongoRepository<BlogJpo, String> {
    BlogJpo findByUserId(String userId);
}
