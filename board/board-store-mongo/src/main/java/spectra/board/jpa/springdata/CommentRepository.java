package spectra.board.jpa.springdata;

import org.springframework.data.mongodb.repository.MongoRepository;
import spectra.board.jpa.jpo.CommentJpo;

import java.util.List;

public interface CommentRepository extends MongoRepository<CommentJpo, String> {
    List<CommentJpo> findByPostId(String postId);
}
