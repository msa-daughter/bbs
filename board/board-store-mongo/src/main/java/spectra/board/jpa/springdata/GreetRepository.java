package spectra.board.jpa.springdata;

import org.springframework.data.mongodb.repository.MongoRepository;
import spectra.board.jpa.jpo.GreetJpo;

public interface GreetRepository extends MongoRepository<GreetJpo, String>{
    GreetJpo findById(String id);
}
