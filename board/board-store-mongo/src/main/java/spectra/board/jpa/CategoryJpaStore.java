package spectra.board.jpa;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import spectra.board.domain.entity.Category;
import spectra.board.domain.store.CategoryStore;
import spectra.board.jpa.jpo.CategoryJpo;
import spectra.board.jpa.springdata.CategoryRepository;
import spectra.share.exception.store.AlreadyExistsException;

import java.util.List;
import java.util.NoSuchElementException;

@Repository
public class CategoryJpaStore implements CategoryStore {

    @Autowired
    private CategoryRepository categoryRepository;

    @Override
    public String create(Category category) {
        String id = category.getId();
        if(categoryRepository.exists(id)) throw new AlreadyExistsException(String.format("Category jpo[ID:%s] already exist.", id));

        CategoryJpo categoryJpo = CategoryJpo.toJpo(category);
        categoryRepository.save(categoryJpo);
        return id;
    }

    @Override
    public Category retrieve(String id) throws NoSuchElementException {
        CategoryJpo categoryJpo =  categoryRepository.findOne(id);
        if (categoryJpo == null)
            throw new NoSuchElementException(String.format("No category jpo[ID:%s] to retrieve.", id));

        return categoryJpo.toDomain();
    }

    @Override
    public List<Category> retrieveByBlogId(String blogId) {
        List<CategoryJpo> categoryJpos = categoryRepository.findByBlogId(blogId);
        return CategoryJpo.toDomains(categoryJpos);
    }

    @Override
    public void update(Category category) {
        CategoryJpo categoryJpo = CategoryJpo.toJpo(category);
        categoryRepository.save(categoryJpo);
    }

    @Override
    public void delete(Category category) {
        delete(category.getId());
    }

    @Override
    public void delete(String id) {
        categoryRepository.delete(id);
    }
}
