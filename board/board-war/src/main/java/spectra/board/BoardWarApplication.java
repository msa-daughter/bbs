package spectra.board;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.builder.SpringApplicationBuilder;
import org.springframework.boot.web.support.SpringBootServletInitializer;

@SpringBootApplication
public class BoardWarApplication extends SpringBootServletInitializer {
	//
	@Override
	protected SpringApplicationBuilder configure(SpringApplicationBuilder application) {
		//
		return application
				.properties(
						"spring.config.location:classpath:/board.properties,classpath:/common.properties"
				)
				.sources(BoardWarApplication.class);
	}

	public static void main(String[] args) {
		//
		SpringApplication.run(SpringApplicationBuilder.class, args);
	}

}
