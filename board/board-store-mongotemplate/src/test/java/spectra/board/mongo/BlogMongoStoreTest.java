package spectra.board.mongo;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.annotation.DirtiesContext;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import spectra.board.domain.entity.Blog;
import spectra.board.domain.store.BlogStore;

@RunWith(SpringJUnit4ClassRunner.class)
@SpringBootTest(classes = BoardStoreTestApplication.class)
//@ComponentScan(basePackages = "spectra.board.mongo")
@DirtiesContext(classMode = DirtiesContext.ClassMode.AFTER_EACH_TEST_METHOD)
public class BlogMongoStoreTest {
	//
	@Autowired
	private BlogStore blogStore;

	String id = null;

	@Before
	public void setUp() {
		//
		Blog blog = new Blog("블러그1", "userid1", "이름1");
		id = blogStore.create(blog);
	}

	@Test
	public void testCreate() {
		Blog blog = new Blog("블러그2", "userid2", "이름2");
		id = blogStore.create(blog);
		Assert.assertTrue(true);
	}

	@Test
	public void testRetrieve() {
		//
		Blog blog = blogStore.retrieve(id);
        Assert.assertTrue(true);
    }

	@Test
	public void testRetrieveByUserId() {
		//
		Blog blog = blogStore.retrieveByUserId("userid1");
		System.out.println(">>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>> " + blog.getBlogName());
		Assert.assertTrue(true);
	}
}
