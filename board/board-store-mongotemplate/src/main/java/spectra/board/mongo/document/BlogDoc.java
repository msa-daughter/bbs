package spectra.board.mongo.document;

import org.springframework.data.annotation.Id;
import org.springframework.data.annotation.Version;
import org.springframework.data.mongodb.core.mapping.Document;
import spectra.board.domain.entity.Blog;

import java.util.List;
import java.util.stream.Collectors;

@Document(collection = "T_BLOG")
//@CompoundIndexes({
//        @CompoundIndex(name = "idx_citizen_name",
//                unique = true,
//                def = "{'metroId' : 1, 'name' : 1}"),
//        @CompoundIndex(name = "idx_citizen_email",
//                unique = true,
//                def = "{'metroId' : 1, 'email' : 1}")
//})
public class BlogDoc {
    @Id
    private String id;

    private String blogName;
    private String description;
    private String userId;
    private String userName;
    private String createdDate;

    @Version
    private Long entityVersion;

    public BlogDoc() {
        //
    }

    public static BlogDoc toDocument(Blog blog) {
        BlogDoc blogDoc = new BlogDoc();
        blogDoc.setId(blog.getId());
        blogDoc.setBlogName(blog.getBlogName());
        blogDoc.setDescription(blog.getDescription());
        blogDoc.setUserId(blog.getUserId());
        blogDoc.setUserName(blog.getUserName());
        blogDoc.setCreatedDate(blog.getCreatedDate());
        blogDoc.setEntityVersion(blog.getEntityVersion());
        return blogDoc;
    }

    public Blog toDomain() {
        Blog blog = new Blog(id);
        blog.setBlogName(blogName);
        blog.setDescription(description);
        blog.setUserId(userId);
        blog.setUserName(userName);
        blog.setCreatedDate(createdDate);
        blog.setEntityVersion(entityVersion);
        return blog;
    }

    public static List<Blog> toDomains(List<BlogDoc> blogDocs){
        return blogDocs.stream()
                .map(jpo->jpo.toDomain())
                .collect(Collectors.toList());
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getBlogName() {
        return blogName;
    }

    public void setBlogName(String blogName) {
        this.blogName = blogName;
    }

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getCreatedDate() {
        return createdDate;
    }

    public void setCreatedDate(String createdDate) {
        this.createdDate = createdDate;
    }

    public Long getEntityVersion() {
        return entityVersion;
    }

    public void setEntityVersion(Long entityVersion) {
        this.entityVersion = entityVersion;
    }

}
