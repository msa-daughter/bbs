package spectra.board.mongo;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.data.mongodb.core.query.Update;
import org.springframework.stereotype.Repository;
import spectra.board.domain.entity.Post;
import spectra.board.domain.store.PostStore;
import spectra.board.mongo.document.PostDoc;
import spectra.share.exception.store.AlreadyExistsException;

import java.util.List;
import java.util.NoSuchElementException;

@Repository
public class PostMongoStore implements PostStore {
    //
    @Autowired
    private MongoTemplate mongoTemplate;

    @Override
    public String create(Post post) {
        //
        String id = post.getId();

        Query query = new Query(Criteria.where("id").is(id));
        PostDoc postDoc = mongoTemplate.findOne(query, PostDoc.class);
        if (postDoc != null) {
            throw new AlreadyExistsException(String.format("Post document[ID:%s] already exist.", id));
        }

        mongoTemplate.save(PostDoc.toDocument(post));
        return id;
    }

    @Override
    public Post retrieve(String id) throws NoSuchElementException {
        Query query = new Query(Criteria.where("id").is(id));
        PostDoc postDoc = mongoTemplate.findOne(query, PostDoc.class);

        if (postDoc == null) {
            throw new NoSuchElementException(String.format("No post[%s] to retrieve.", id));
        }

        return postDoc.toDomain();
    }

    @Override
    public List<Post> retrieveByBlogId(String blogId, int offset, int limit) {
        Query query = new Query(Criteria.where("blogId").is(blogId));
        PostDoc postDoc = mongoTemplate.findOne(query, PostDoc.class);

        query.skip(offset);
        query.limit(limit);
        List<PostDoc> postDocs = mongoTemplate.find(query, PostDoc.class);

        return postDoc.toDomains(postDocs);
    }

    @Override
    public List<Post> retrieveByCategoryId(String categoryId, int offset, int limit) {
        Query query = new Query(Criteria.where("categoryId").is(categoryId));

        query.skip(offset);
        query.limit(limit);
        List<PostDoc> postDocs = mongoTemplate.find(query, PostDoc.class);

        return PostDoc.toDomains(postDocs);
    }

    @Override
    public List<Post> retrieveByContent(String content, int offset, int limit) {
        Query query = new Query(Criteria.where("content").is(content));

        query.skip(offset);
        query.limit(limit);
        List<PostDoc> postDocs = mongoTemplate.find(query, PostDoc.class);

        return PostDoc.toDomains(postDocs);
    }

    @Override
    public List<Post> retrieveByTitle(String title, int offset, int limit) {
        Query query = new Query(Criteria.where("title").regex(title));

        query.skip(offset);
        query.limit(limit);
        List<PostDoc> postDocs = mongoTemplate.find(query, PostDoc.class);

        return PostDoc.toDomains(postDocs);
    }

    @Override
    public List<Post> retrieveByTitleAndContent(String title, String content, int offset, int limit) {
        Query query = new Query(Criteria.where("title").is(title).andOperator(Criteria.where("content").is(content)));

        query.skip(offset);
        query.limit(limit);
        List<PostDoc> postDocs = mongoTemplate.find(query, PostDoc.class);

        return PostDoc.toDomains(postDocs);
    }

    @Override
    public void update(Post post) {
        //
        Update update = new Update();
        update.set("blogId", post.getBlogId());
        update.set("categoryId", post.getCategoryId());
        update.set("content", post.getContent());
        update.set("tagNames", post.getTagNames());
        update.set("title", post.getTitle());
        update.set("updateDate", post.getUpdatedDate());

        String id = post.getId();
        Query query = new Query(Criteria.where("id").is(id));

        mongoTemplate.updateFirst(query, update, PostDoc.class);
    }

    @Override
    public void delete(Post post) {
        delete(post.getId());
    }

    @Override
    public void delete(String id) {
        //
        Query query = new Query();
        query.addCriteria(Criteria.where("id").is(id));
        mongoTemplate.remove(query, PostDoc.class);

    }
}
