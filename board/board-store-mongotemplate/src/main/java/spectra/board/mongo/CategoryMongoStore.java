package spectra.board.mongo;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.data.mongodb.core.query.Update;
import org.springframework.stereotype.Repository;
import spectra.board.domain.entity.Category;
import spectra.board.domain.store.CategoryStore;
import spectra.board.mongo.document.CategoryDoc;
import spectra.share.exception.store.AlreadyExistsException;

import java.util.List;
import java.util.NoSuchElementException;

@Repository
public class CategoryMongoStore implements CategoryStore {
    //
    @Autowired
    private MongoTemplate mongoTemplate;

    @Override
    public String create(Category category) {
        //
        String id = category.getId();

        Query query = new Query(Criteria.where("id").is(id));
        CategoryDoc categoryDoc = mongoTemplate.findOne(query, CategoryDoc.class);
        if (categoryDoc != null) {
            throw new AlreadyExistsException(String.format("Category document[ID:%s] already exist.", id));
        }

        mongoTemplate.save(CategoryDoc.toDocument(category));
        return id;
    }

    @Override
    public Category retrieve(String id) throws NoSuchElementException {
        //
        Query query = new Query(Criteria.where("id").is(id));
        CategoryDoc categoryDoc = mongoTemplate.findOne(query, CategoryDoc.class);

        if (categoryDoc == null) {
            throw new NoSuchElementException(String.format("No category[%s] to retrieve.", id));
        }

        return categoryDoc.toDomain();
    }

    @Override
    public List<Category> retrieveByBlogId(String blogId) {
        Query query = new Query(Criteria.where("blogId").is(blogId));
        List<CategoryDoc> categoryDocs = mongoTemplate.find(query, CategoryDoc.class);

        return CategoryDoc.toDomains(categoryDocs);
    }

    @Override
    public void update(Category category) {
        //
        Update update = new Update();
        update.set("blogId", category.getBlogId());
        update.set("categoryName", category.getCategoryName());
        update.set("useCount", category.getUseCount());

        String id = category.getId();
        Query query = new Query(Criteria.where("id").is(id));

        mongoTemplate.updateFirst(query, update, CategoryDoc.class);
    }

    @Override
    public void delete(Category category) {
        //
        delete(category.getId());
    }

    @Override
    public void delete(String Id) {
        //
        Query query = new Query();
        query.addCriteria(Criteria.where("id").is(Id));
        mongoTemplate.remove(query, CategoryDoc.class);
    }
}
