package spectra.board.mongo.document;

import org.springframework.data.mongodb.core.mapping.Document;
import spectra.board.domain.entity.Greet;

import javax.persistence.Id;
import java.util.List;
import java.util.stream.Collectors;

@Document(collection = "T_GREET")
public class GreetDoc{
    @Id
    private String id;
    private String message;

    public GreetDoc(){
        //
    }

    public GreetDoc(String id, String message){
        this.id = id;
        this.message = message;
    }


    public static GreetDoc toDocument(Greet greet){
        GreetDoc greetDoc = new GreetDoc();
        greetDoc.setId(greet.getId());
        greetDoc.setMessage(greet.getMessage());
        return greetDoc;
    }

    public Greet toDomain(){
        Greet greet = new Greet(id);
        greet.setMessage(message);
        return greet;
    }

    public static List<Greet> toDomains(List<GreetDoc> greetJpos){
        return greetJpos.stream()
                .map(jpo->jpo.toDomain())
                .collect(Collectors.toList());
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }
}
