package spectra.board.mongo;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.data.mongodb.core.query.Update;
import org.springframework.stereotype.Repository;
import spectra.board.domain.entity.Comment;
import spectra.board.domain.store.CommentStore;
import spectra.board.mongo.document.CommentDoc;
import spectra.share.exception.store.AlreadyExistsException;

import java.util.List;
import java.util.NoSuchElementException;

@Repository
public class CommentMongoStore implements CommentStore {
    //
    @Autowired
    private MongoTemplate mongoTemplate;

    @Override
    public String create(Comment comment) {
        //
        String id = comment.getId();

        Query query = new Query(Criteria.where("id").is(id));
        CommentDoc commentDoc = mongoTemplate.findOne(query, CommentDoc.class);
        if (commentDoc != null) {
            throw new AlreadyExistsException(String.format("Comment document[ID:%s] already exist.", id));
        }

        mongoTemplate.save(CommentDoc.toDocument(comment));
        return id;
    }

    @Override
    public Comment retrieve(String id) throws NoSuchElementException {
        Query query = new Query(Criteria.where("id").is(id));
        CommentDoc commentDoc = mongoTemplate.findOne(query, CommentDoc.class);

        if (commentDoc == null) {
            throw new NoSuchElementException(String.format("No comment[%s] to retrieve.", id));
        }

        return commentDoc.toDomain();
    }

    @Override
    public List<Comment> retrieveByPostId(String postId) {
        Query query = new Query(Criteria.where("postId").is(postId));

        List<CommentDoc> commentDocs = mongoTemplate.find(query, CommentDoc.class);

        return CommentDoc.toDomains(commentDocs);
    }

    @Override
    public void update(Comment comment) {
        //
        Update update = new Update();
        update.set("content", comment.getContent());
        update.set("userId", comment.getUserId());
        update.set("userName", comment.getUserName());

        String id = comment.getId();
        Query query = new Query(Criteria.where("id").is(id));

        mongoTemplate.updateFirst(query, update, CommentDoc.class);
    }

    @Override
    public void delete(Comment comment) {
        delete(comment.getId());
    }

    @Override
    public void delete(String id) {
        //
        Query query = new Query();
        query.addCriteria(Criteria.where("id").is(id));
        mongoTemplate.remove(query, CommentDoc.class);
    }
}
