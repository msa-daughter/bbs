package spectra.board.mongo.document;

import org.springframework.data.annotation.Id;
import org.springframework.data.annotation.Version;
import org.springframework.data.mongodb.core.mapping.Document;
import spectra.board.domain.entity.Category;

import java.util.List;
import java.util.stream.Collectors;

@Document(collection = "T_CATEGORY")
public class CategoryDoc {
    @Id
    private String id;

    private String categoryName;
    private int useCount;
    private String createdDate;
    private String blogId;

    @Version
    private Long entiryVersion;

    public CategoryDoc() {
        //
    }

    public static CategoryDoc toDocument(Category category)
    {
        CategoryDoc categoryDoc = new CategoryDoc();
        categoryDoc.setId(category.getId());
        categoryDoc.setCategoryName(category.getCategoryName());
        categoryDoc.setUseCount(category.getUseCount());
        categoryDoc.setCreatedDate(category.getCreatedDate());
        categoryDoc.setBlogId(category.getBlogId());
        categoryDoc.setEntiryVersion(category.getEntityVersion());
        return categoryDoc;
    }

    public Category toDomain()
        {
        Category category = new Category(id);
        category.setCategoryName(categoryName);
        category.setUseCount(useCount);
        category.setCreatedDate(createdDate);
        category.setBlogId(blogId);
        category.setEntityVersion(entiryVersion);
        return category;
    }

    public static List<Category> toDomains(List<CategoryDoc> categoryDoc){
        return categoryDoc.stream()
                .map(jpos->jpos.toDomain())
                .collect(Collectors.toList());
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getCategoryName() {
        return categoryName;
    }

    public void setCategoryName(String categoryName) {
        this.categoryName = categoryName;
    }

    public int getUseCount() {
        return useCount;
    }

    public void setUseCount(int useCount) {
        this.useCount = useCount;
    }

    public String getCreatedDate() {
        return createdDate;
    }

    public void setCreatedDate(String createdDate) {
        this.createdDate = createdDate;
    }

    public String getBlogId() {
        return blogId;
    }

    public void setBlogId(String blogId) {
        this.blogId = blogId;
    }

    public Long getEntiryVersion() {
        return entiryVersion;
    }

    public void setEntiryVersion(Long entiryVersion) {
        this.entiryVersion = entiryVersion;
    }
}
