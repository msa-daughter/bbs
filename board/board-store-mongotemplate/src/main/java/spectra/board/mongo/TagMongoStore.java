package spectra.board.mongo;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.data.mongodb.core.query.Update;
import org.springframework.stereotype.Repository;
import spectra.board.domain.entity.Tag;
import spectra.board.domain.store.TagStore;
import spectra.board.mongo.document.TagDoc;
import spectra.share.exception.store.AlreadyExistsException;

import java.util.List;
import java.util.NoSuchElementException;

@Repository
public class TagMongoStore implements TagStore {
    //
    @Autowired
    private MongoTemplate mongoTemplate;

    @Override
    public String create(Tag tag) {
        //
        String id = tag.getId();

        Query query = new Query(Criteria.where("id").is(id));
        TagDoc tagDoc = mongoTemplate.findOne(query, TagDoc.class);
        if (tagDoc != null) {
            throw new AlreadyExistsException(String.format("Tag document[ID:%s] already exist.", id));
        }

        mongoTemplate.save(TagDoc.toDocument(tag));
        return id;
    }

    @Override
    public Tag retrieve(String id) throws NoSuchElementException {
        Query query = new Query(Criteria.where("id").is(id));
        TagDoc tagDoc = mongoTemplate.findOne(query, TagDoc.class);

        if (tagDoc == null) {
            throw new NoSuchElementException(String.format("No tag[%s] to retrieve.", id));
        }

        return tagDoc.toDomain();
    }

    @Override
    public Tag retrieveByTagName(String tagName) {
        Query query = new Query(Criteria.where("tagName").is(tagName));
        TagDoc tagDoc = mongoTemplate.findOne(query, TagDoc.class);

        return tagDoc == null ? null : tagDoc.toDomain();
    }

    @Override
    public List<Tag> retrieveByBlogId(String blogId, int offset, int limit) {
        Query query = new Query(Criteria.where("blogId").is(blogId));

        query.skip(offset);
        query.limit(limit);
        List<TagDoc> tagDocs = mongoTemplate.find(query, TagDoc.class);

        return TagDoc.toDomains(tagDocs);
    }

    @Override
    public void update(Tag tag) {
        //
        Update update = new Update();
        update.set("useCount", tag.getUseCount());
        update.set("tagName", tag.getTagName());
        update.set("postIds", tag.getPostIds());

        String id = tag.getId();
        Query query = new Query(Criteria.where("id").is(id));

        mongoTemplate.updateFirst(query, update, TagDoc.class);
    }
}
