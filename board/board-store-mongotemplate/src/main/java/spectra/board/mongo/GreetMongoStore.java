package spectra.board.mongo;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.data.mongodb.core.query.Update;
import org.springframework.stereotype.Repository;
import spectra.board.domain.entity.Greet;
import spectra.board.domain.store.GreetStore;
import spectra.board.mongo.document.GreetDoc;
import spectra.share.exception.store.AlreadyExistsException;

import java.util.NoSuchElementException;

@Repository
public class GreetMongoStore implements GreetStore {
    //
    @Autowired
    private MongoTemplate mongoTemplate;

    @Override
    public String create(Greet greet) {
        //
        String id = greet.getId();

        Query query = new Query(Criteria.where("id").is(id));
        GreetDoc greetDoc = mongoTemplate.findOne(query, GreetDoc.class);
        if (greetDoc != null) {
            throw new AlreadyExistsException(String.format("Greet document[ID:%s] already exist.", id));
        }

        mongoTemplate.save(GreetDoc.toDocument(greet));
        return id;
    }

    @Override
    public Greet retrieve(String id) throws NoSuchElementException {
        Query query = new Query(Criteria.where("id").is(id));
        GreetDoc greetDoc = mongoTemplate.findOne(query, GreetDoc.class);

        if (greetDoc == null) {
            throw new NoSuchElementException(String.format("No greet[%s] to retrieve.", id));
        }

        return greetDoc.toDomain();
    }

    @Override
    public void update(Greet greet) {
        //
        Update update = new Update();
        update.set("message", greet.getMessage());

        String id = greet.getId();
        Query query = new Query(Criteria.where("id").is(id));

        mongoTemplate.updateFirst(query, update, GreetDoc.class);

    }

    @Override
    public void delete(Greet greet) {
        delete(greet.getId());
    }

    @Override
    public void delete(String id) {
        //
        Query query = new Query();
        query.addCriteria(Criteria.where("id").is(id));
        mongoTemplate.remove(query, GreetDoc.class);
    }
}
