package spectra.board.mongo.document;

import org.springframework.data.annotation.Id;
import org.springframework.data.annotation.Version;
import org.springframework.data.mongodb.core.mapping.Document;
import spectra.board.domain.entity.Tag;

import java.util.List;
import java.util.Objects;
import java.util.stream.Collectors;

@Document(collection = "T_TAG")
public class TagDoc {
	//
	@Id
	private String id;

	private String tagName;
	private int useCount;
	private String createdDate;

	private String blogId;

	private List<String> postIds;

	@Version
	private Long entiryVersion;

	public TagDoc() {
	    //
	}

	public static TagDoc toDocument(Tag tag){
	    TagDoc tagDoc = new TagDoc();
	    tagDoc.setId(tag.getId());
        tagDoc.setTagName(tag.getTagName());
        tagDoc.setUseCount(tag.getUseCount());
        tagDoc.setCreatedDate(tag.getCreatedDate());
        tagDoc.setBlogId(tag.getBlogId());
        tagDoc.setPostIds(tag.getPostIds());
        tagDoc.setEntiryVersion(tag.getEntityVersion());

        return tagDoc;
    }

    public Tag toDomain(){
        Tag tag = new Tag(id);
        tag.setTagName(tagName);
        tag.setUseCount(useCount);
        tag.setCreatedDate(createdDate);
        tag.setBlogId(blogId);
        tag.setPostIds(postIds);
        return tag;
    }

	public static List<Tag> toDomains(List<TagDoc> tagDocs){
		return tagDocs.stream()
				.map(jpos->jpos.toDomain())
				.collect(Collectors.toList());
	}

    @Override
	public boolean equals(Object o) {
		if (this == o) return true;
		if (o == null || getClass() != o.getClass()) return false;
		TagDoc tagDoc = (TagDoc) o;
		return Objects.equals(tagName, tagDoc.tagName);
	}

	@Override
	public int hashCode() {
		return Objects.hash(tagName);
	}

    //getter and setter
	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getTagName() {
		return tagName;
	}

	public void setTagName(String tagName) {
		this.tagName = tagName;
	}

	public int getUseCount() {
		return useCount;
	}

	public void setUseCount(int useCount) {
		this.useCount = useCount;
	}

	public String getCreatedDate() {
		return createdDate;
	}

	public void setCreatedDate(String createdDate) {
		this.createdDate = createdDate;
	}

	public String getBlogId() {
		return blogId;
	}

	public void setBlogId(String blogId) {
		this.blogId = blogId;
	}

	public List<String> getPostIds() {
		return postIds;
	}

	public void setPostIds(List<String> postIds) {
		this.postIds = postIds;
	}

	public Long getEntiryVersion() {
		return entiryVersion;
	}

	public void setEntiryVersion(Long entiryVersion) {
		this.entiryVersion = entiryVersion;
	}
}
