package spectra.board.mongo.document;

import org.springframework.data.annotation.Id;
import org.springframework.data.annotation.Version;
import org.springframework.data.mongodb.core.mapping.Document;
import spectra.board.domain.entity.Post;

import java.util.List;
import java.util.stream.Collectors;

@Document(collection="T_POST")
public class PostDoc {
    //
    @Id
    private String id;

    private String title;
    private String content;
    private String createdDate;
    private String updatedDate;

    private String blogId;
    private String categoryId;

    private List<String> tagNames;

    @Version
    private Long entityVersion;

    public PostDoc(){
        //
    }

    public static PostDoc toDocument(Post post){
        PostDoc postDoc = new PostDoc();
        postDoc.setId(post.getId());
        postDoc.setTitle(post.getTitle());
        postDoc.setContent(post.getContent());
        postDoc.setCreatedDate(post.getCreatedDate());
        postDoc.setUpdatedDate(post.getUpdatedDate());
        postDoc.setBlogId(post.getBlogId());
        postDoc.setCategoryId(post.getCategoryId());
        postDoc.setTagNames(post.getTagNames());
        postDoc.setEntityVersion(post.getEntityVersion());
        return postDoc;
    }

    public Post toDomain(){
        Post post = new Post(id);
        post.setTitle(title);
        post.setContent(content);
        post.setCreatedDate(createdDate);
        post.setUpdatedDate(updatedDate);
        post.setBlogId(blogId);
        post.setCategoryId(categoryId);
        post.setTagNames(tagNames);
        post.setEntityVersion(entityVersion);
        return post;
    }

    public static List<Post> toDomains(List<PostDoc> postDocs){
        return postDocs.stream()
                .map(jpos->jpos.toDomain())
                .collect(Collectors.toList());
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof PostDoc)) return false;
        return id != null && id.equals(((PostDoc) o).id);
    }

    @Override
    public int hashCode() {
        return 31;
    }

    //getter and setter

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }

    public String getCreatedDate() {
        return createdDate;
    }

    public void setCreatedDate(String createdDate) {
        this.createdDate = createdDate;
    }

    public String getUpdatedDate() {
        return updatedDate;
    }

    public void setUpdatedDate(String updatedDate) {
        this.updatedDate = updatedDate;
    }

    public String getBlogId() {
        return blogId;
    }

    public void setBlogId(String blogId) {
        this.blogId = blogId;
    }

    public String getCategoryId() {
        return categoryId;
    }

    public void setCategoryId(String categoryId) {
        this.categoryId = categoryId;
    }

    public List<String> getTagNames() {
        return tagNames;
    }

    public void setTagNames(List<String> tagNames) {
        this.tagNames = tagNames;
    }

    public Long getEntityVersion() {
        return entityVersion;
    }

    public void setEntityVersion(Long entityVersion) {
        this.entityVersion = entityVersion;
    }
}
