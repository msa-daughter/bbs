package spectra.board.mongo.document;

import org.springframework.data.annotation.Id;
import org.springframework.data.annotation.Version;
import org.springframework.data.mongodb.core.mapping.Document;
import spectra.board.domain.entity.Comment;

import java.util.List;
import java.util.stream.Collectors;

@Document(collection = "T_COMMENT")
public class CommentDoc {
    //
    @Id
    private String id;

    private String content;
    private String createdDate;

    private String postId;
    private String userId;
    private String userName;

    @Version
    private Long entityVersion;

    public CommentDoc() {
        //
    }

    public static CommentDoc toDocument(Comment comment) {
        CommentDoc blogDoc = new CommentDoc();
        blogDoc.setId(comment.getId());
        blogDoc.setContent(comment.getContent());
        blogDoc.setCreatedDate(comment.getCreatedDate());
        blogDoc.setPostId(comment.getPostId());
        blogDoc.setUserId(comment.getUserId());
        blogDoc.setUserName(comment.getUserName());
        blogDoc.setEntityVersion(comment.getEntityVersion());
        return blogDoc;
    }

    public Comment toDomain() {
        Comment comment = new Comment(id);
        comment.setContent(content);
        comment.setCreatedDate(createdDate);
        comment.setPostId(postId);
        comment.setUserId(userId);
        comment.setUserName(userName);
        comment.setEntityVersion(entityVersion);
        return comment;
    }

    public static List<Comment> toDomains(List<CommentDoc> commentDocs){
        return commentDocs.stream()
                .map(jpo->jpo.toDomain())
                .collect(Collectors.toList());
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }

    public String getCreatedDate() {
        return createdDate;
    }

    public void setCreatedDate(String createdDate) {
        this.createdDate = createdDate;
    }

    public String getPostId() {
        return postId;
    }

    public void setPostId(String postId) {
        this.postId = postId;
    }

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public Long getEntityVersion() {
        return entityVersion;
    }

    public void setEntityVersion(Long entityVersion) {
        this.entityVersion = entityVersion;
    }
}
