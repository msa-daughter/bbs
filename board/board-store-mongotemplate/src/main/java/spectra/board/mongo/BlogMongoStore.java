package spectra.board.mongo;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.data.mongodb.core.query.Update;
import org.springframework.stereotype.Repository;
import spectra.board.domain.entity.Blog;
import spectra.board.domain.store.BlogStore;
import spectra.board.mongo.document.BlogDoc;
import spectra.share.exception.store.AlreadyExistsException;

import java.util.NoSuchElementException;

@Repository
public class BlogMongoStore implements BlogStore {
    //
    @Autowired
    private MongoTemplate mongoTemplate;

    @Override
    public String create(Blog blog) {
        //
        String id = blog.getId();

        Query query = new Query(Criteria.where("id").is(id));
        BlogDoc blogDoc = mongoTemplate.findOne(query, BlogDoc.class);
        if (blogDoc != null) {
            throw new AlreadyExistsException(String.format("Blog document[ID:%s] already exist.", id));
        }

        mongoTemplate.save(BlogDoc.toDocument(blog));
        return id;
    }

    @Override
    public Blog retrieve(String id) throws NoSuchElementException {
        Query query = new Query(Criteria.where("id").is(id));
        BlogDoc blogDoc = mongoTemplate.findOne(query, BlogDoc.class);

        if (blogDoc == null) {
            throw new NoSuchElementException(String.format("No blog[%s] to retrieve.", id));
        }

        return blogDoc.toDomain();
    }

    @Override
    public Blog retrieveByUserId(String userId) {
        Query query = new Query(Criteria.where("userId").is(userId));
        BlogDoc blogDoc = mongoTemplate.findOne(query, BlogDoc.class);

        return blogDoc == null ? null : blogDoc.toDomain();
    }

    @Override
    public void update(Blog blog) {
        //
        Update update = new Update();
        update.set("blogName", blog.getBlogName());
        update.set("description", blog.getDescription());
        update.set("userId", blog.getUserId());
        update.set("userName", blog.getUserName());

        String id = blog.getId();
        Query query = new Query(Criteria.where("id").is(id));

        mongoTemplate.updateFirst(query, update, BlogDoc.class);
    }

    @Override
    public void delete(String blogId) {
        //
        Query query = new Query();
        query.addCriteria(Criteria.where("id").is(blogId));
        mongoTemplate.remove(query, BlogDoc.class);
    }

    @Override
    public void delete(Blog blog) {
        //
        delete(blog.getId());
    }
}
