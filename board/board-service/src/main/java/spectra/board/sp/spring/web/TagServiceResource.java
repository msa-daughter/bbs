package spectra.board.sp.spring.web;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.*;
import spectra.board.domain.entity.Tag;
import spectra.board.domain.spec.TagService;
import spectra.board.domain.spec.sdo.TagCdo;

import java.util.List;

@Transactional
@RestController
@CrossOrigin(origins = "*")
@RequestMapping("api/s/tags")
public class TagServiceResource implements TagService {

    @Autowired
    @Qualifier("tagLogic")
    private TagService tagService;

    @Override
    @PostMapping
    public String registerTag(@RequestBody TagCdo tagCdo) {
        return tagService.registerTag(tagCdo);
    }

    @Override
    @GetMapping(value = "tag/{blogId}", params = {"offset", "limit"})
    public List<Tag> findTagByBlogId(@PathVariable String blogId, @RequestParam int offset, @RequestParam int limit) {
        return tagService.findTagByBlogId(blogId, offset, limit);
    }
}
