package spectra.board.sp.spring.web;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import spectra.board.domain.entity.Greet;
import spectra.board.domain.spec.GreetProvider;
import spectra.board.domain.spec.GreetService;

@Transactional
@RestController
@RequestMapping("api/p/greet-message")
public class GreetProviderResource implements GreetProvider {

    @Autowired
    @Qualifier("greetLogic")
    private GreetService greetService;

    @Override
    @GetMapping("{messageId}")
    public Greet sayHello(@PathVariable String messageId) {
        return  greetService.sayHello(messageId);
    }
}
