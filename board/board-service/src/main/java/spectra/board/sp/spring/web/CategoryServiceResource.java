package spectra.board.sp.spring.web;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.*;
import spectra.board.domain.entity.Category;
import spectra.board.domain.spec.CategoryService;
import spectra.board.domain.spec.sdo.CategoryCdo;
import spectra.share.domain.NameValueList;

import java.util.List;

@Transactional
@RestController
@CrossOrigin(origins = "*")
@RequestMapping("api/s/categories")
public class CategoryServiceResource implements CategoryService {

    @Autowired
    @Qualifier("categoryLogic")
    private CategoryService categoryService;

    @Override
    @PostMapping
    public String registerCategory(@RequestBody CategoryCdo categoryCdo) {
        return categoryService.registerCategory(categoryCdo);
    }

    @Override
    @GetMapping("{categoryId}")
    public Category findCategory(@PathVariable String categoryId) {
        return categoryService.findCategory(categoryId);
    }

    @Override
    @GetMapping(params = "blogId")
    public List<Category> findCategoryByBlogId(@RequestParam String blogId) {
        return categoryService.findCategoryByBlogId(blogId);
    }

    @Override
    @GetMapping(value = "users/{userId}")
    public List<Category> findCategoryByUserId(@PathVariable String userId) {
        return categoryService.findCategoryByUserId(userId);
    }

    @Override
    @PostMapping("{categoryId}")
    public void modifyCategory(@PathVariable String categoryId, @RequestBody NameValueList nameValues) {
        categoryService.modifyCategory(categoryId, nameValues);
    }

    @Override
    @DeleteMapping("{categoryId}")
    public void removeCategory(@PathVariable String categoryId) {
        categoryService.removeCategory(categoryId);
    }
}
