package spectra.board.sp.spring.web;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.*;
import spectra.board.domain.entity.Post;
import spectra.board.domain.spec.PostService;
import spectra.board.domain.spec.sdo.PostCdo;
import spectra.share.domain.NameValueList;

import java.util.List;

@Transactional
@RestController
@CrossOrigin(origins = "*")
@RequestMapping("api/s/posts")
public class PostServiceResource implements PostService {

    @Autowired
    @Qualifier("postLogic")
    private PostService postService;

    @Override
    @PostMapping
    public String registerPost(@RequestBody PostCdo postCdo) {
        return postService.registerPost(postCdo);
    }

    @Override
    @GetMapping("{postId}")
    public Post findPost(@PathVariable String postId) {
        return postService.findPost(postId);
    }


    @Override
    @GetMapping(value = "blog/{blogId}", params = {"offset", "limit"})
    public List<Post> findPostByBlogId(@PathVariable String blogId, @RequestParam int offset, @RequestParam int limit) {
        return postService.findPostByBlogId(blogId, offset, limit);
    }

    @Override
    @GetMapping(value = "user/{userId}", params = {"offset", "limit"})
    public List<Post> findPostByUserId(@PathVariable String userId, @RequestParam int offset, @RequestParam int limit) {
        return postService.findPostByUserId(userId, offset, limit);
    }

    @Override
    @GetMapping(value = "category/{categoryId}", params = {"offset", "limit"})
    public List<Post> findPostByCategoryId(@PathVariable String categoryId, @RequestParam int offset, @RequestParam int limit) {
        return postService.findPostByCategoryId(categoryId, offset, limit);
    }

    @Override
    @GetMapping(value = "tag", params = {"tagName", "offset", "limit"})
    public List<Post> findPostByTagName(@RequestParam String tagName, @RequestParam int offset, @RequestParam int limit) {
        return postService.findPostByTagName(tagName, offset, limit);
    }

    @Override
    @GetMapping(value = "content",params = {"content", "offset", "limit"})
    public List<Post> findPostByContent(@RequestParam String content, @RequestParam int offset, @RequestParam int limit) {
        return postService.findPostByContent(content, offset, limit);
    }

    @Override
    @GetMapping(value = "title",params = {"title","offset", "limit"})
    public List<Post> findPostByTitle(@RequestParam String title,@RequestParam int offset, @RequestParam int limit) {
        return postService.findPostByTitle(title, offset, limit);
    }


    @Override
    @GetMapping(value = "titlencontent",params = {"title", "content","offset", "limit"})
    public List<Post> findPostByTitleAndContent(@RequestParam String title, @RequestParam String content, @RequestParam int offset, @RequestParam int limit) {
        return postService.findPostByTitleAndContent(title, content, offset, limit);
    }

    @Override
    @PostMapping("{postId}")
    public void modifyPost(@PathVariable String postId, @RequestBody NameValueList nameValues) {
        postService.modifyPost(postId, nameValues);
    }

    @Override
    @DeleteMapping("{postId}")
    public void removePost(@PathVariable String postId) {
        postService.removePost(postId);
    }
}
