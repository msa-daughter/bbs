package spectra.board.sp.spring.web;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.*;
import spectra.board.domain.entity.Comment;
import spectra.board.domain.spec.CommentService;
import spectra.board.domain.spec.sdo.CommentCdo;
import spectra.share.domain.NameValueList;

import java.util.List;

@Transactional
@RestController
@CrossOrigin(origins = "*")
@RequestMapping("api/s/comments")
public class CommentServiceResource implements CommentService {
    //
    @Autowired
    @Qualifier("commentLogic")
    private CommentService commentService;

    @Override
    @PostMapping
    public  String registerComment(@RequestBody CommentCdo commentCdo) {
        return commentService.registerComment(commentCdo);
    }

    @Override
    @GetMapping("{commentId}")
    public Comment findComment(@PathVariable String commentId) {
        return commentService.findComment(commentId);
    }

    @Override
    @GetMapping(params = "postId")
    public List<Comment> findCommentByPostId(@RequestParam String postId) {
        return commentService.findCommentByPostId(postId);
    }

    @Override
    @PutMapping("{commentId}")
    public void modifyComment(@PathVariable String commentId, @RequestBody NameValueList nameValues) {
        commentService.modifyComment(commentId, nameValues);
    }

    @Override
    @DeleteMapping("{commentId}")
    public void removeComment(@PathVariable String commentId) {
        commentService.removeComment(commentId);
    }
}
