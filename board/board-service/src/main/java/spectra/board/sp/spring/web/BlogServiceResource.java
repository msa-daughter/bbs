package spectra.board.sp.spring.web;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.*;
import spectra.board.domain.entity.Blog;
import spectra.board.domain.spec.BlogService;
import spectra.board.domain.spec.sdo.BlogCdo;
import spectra.share.domain.NameValueList;

@Transactional
@RestController
@CrossOrigin(origins = "*")
@RequestMapping("api/s/blogs")
public class BlogServiceResource implements BlogService {
    //
    @Autowired
    @Qualifier("blogLogic")
    private BlogService blogService;

    @Override
    @PostMapping
    public  String createBlog(@RequestBody BlogCdo blogCdo) {
        return blogService.createBlog(blogCdo);
    }

    @Override
    @GetMapping("{blogId}")
    public Blog findBlog(@PathVariable String blogId) {
        return blogService.findBlog(blogId);
    }

    @Override
    @GetMapping(params = "userId")
    public Blog findBlogByUserId(@RequestParam String userId) {
        return blogService.findBlogByUserId(userId);
    }

    @Override
    @PutMapping("{blogId}")
    public void modifyBlog(@PathVariable String blogId, @RequestBody NameValueList nameValues) {
        blogService.modifyBlog(blogId, nameValues);
    }

    @Override
    @DeleteMapping("{blogId}")
    public void removeBlog(@PathVariable String blogId) {
        blogService.removeBlog(blogId);
    }
}
