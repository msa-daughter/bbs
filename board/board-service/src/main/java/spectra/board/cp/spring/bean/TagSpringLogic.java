package spectra.board.cp.spring.bean;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import spectra.board.domain.logic.TagLogic;
import spectra.board.domain.store.BoardStoreLycler;

@Service("tagLogic")
public class TagSpringLogic extends TagLogic {

    @Autowired
    public TagSpringLogic(BoardStoreLycler boardStoreLycler) {
        super(boardStoreLycler);
    }
}
