package spectra.board.cp.spring.bean;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import spectra.board.domain.logic.CategoryLogic;
import spectra.board.domain.store.BoardStoreLycler;

@Service("categoryLogic")
public class CategorySpringLogic extends CategoryLogic {

    @Autowired
    public CategorySpringLogic(BoardStoreLycler boardStoreLycler) {
        super(boardStoreLycler);
    }
}
