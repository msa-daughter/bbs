package spectra.board.cp.spring.bean;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import spectra.board.domain.logic.BlogLogic;
import spectra.board.domain.store.BoardStoreLycler;

@Service ("blogLogic")
public class BlogSpringLogic extends BlogLogic {

    @Autowired
    public BlogSpringLogic(BoardStoreLycler storeLycler){
        super (storeLycler);
    }
}
