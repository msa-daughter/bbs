package spectra.board.cp.spring;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import spectra.board.domain.store.*;

@Component
public class BoardStoreSpringLycler implements BoardStoreLycler {

    @Autowired
    private BlogStore blogStore;

    @Autowired
    private CategoryStore categoryStore;

    @Autowired
    private CommentStore commentStore;

    @Autowired
    private PostStore postStore;

    @Autowired
    private TagStore tagStore;

    @Override
    public BlogStore requestBlogStore() {
        return blogStore;
    }

    @Override
    public CategoryStore requestCategoryStore() {
        return categoryStore;
    }

    @Override
    public CommentStore requestCommentStore() {
        return commentStore;
    }

    @Override
    public PostStore requestPostStore() {
        return postStore;
    }

    @Override
    public TagStore requestTagStore() {
        return tagStore;
    }
}
