package spectra.board.cp.spring.bean;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import spectra.board.domain.logic.GreetLogic;
import spectra.board.domain.store.GreetStoreLycler;

@Service ("greetLogic")
public class GreetSpringLogic extends GreetLogic {

    @Autowired
    public GreetSpringLogic(GreetStoreLycler storeLycler){
        super (storeLycler);
    }
}
