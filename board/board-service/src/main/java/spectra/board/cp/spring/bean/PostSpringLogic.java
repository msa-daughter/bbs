package spectra.board.cp.spring.bean;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import spectra.board.domain.logic.PostLogic;
import spectra.board.domain.store.BoardStoreLycler;

@Service("postLogic")
public class PostSpringLogic extends PostLogic {
    //
    @Autowired
    public PostSpringLogic(BoardStoreLycler boardStoreLycler) {
        super(boardStoreLycler);
    }
}
