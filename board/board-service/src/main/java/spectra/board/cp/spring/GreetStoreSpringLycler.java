package spectra.board.cp.spring;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import spectra.board.domain.store.GreetStore;
import spectra.board.domain.store.GreetStoreLycler;

@Component
public class GreetStoreSpringLycler implements GreetStoreLycler {

    @Autowired
    private GreetStore greetStore;

    @Override
    public GreetStore requestGreetStore() {
        return greetStore;
    }
}
