package spectra.board.cp.spring.bean;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import spectra.board.domain.logic.CommentLogic;
import spectra.board.domain.store.BoardStoreLycler;

@Service("commentLogic")
public class CommentSpringLogic extends CommentLogic {
    @Autowired
    public CommentSpringLogic(BoardStoreLycler boardStoreLycler) {
        super(boardStoreLycler);
    }
}
