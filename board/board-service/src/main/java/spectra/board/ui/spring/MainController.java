package spectra.board.ui.spring;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;

/**
 * Created by Dell on 2017-12-05.
 */
@Controller
public class MainController {

    @RequestMapping("/**")
    public String index(
            Model model
    ) {
        //
        //model.addAttribute("commonAssetLocation", commonAssetLocation);
        return "index";
    }

}
