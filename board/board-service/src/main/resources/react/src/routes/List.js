import React from 'react'
import PropTypes from 'prop-types'

import Header from '../components/Header'
import Lists from '../components/Lists'

const List = (props) => {
  return (
    <Header/>
    <Lists />
  )
}

export default List
