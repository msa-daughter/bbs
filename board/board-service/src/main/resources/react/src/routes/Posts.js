import React from 'react'
import PropTypes from 'prop-types'
import { Route, Link } from 'react-router-dom'


const Post = ({match}) => {
    return (
      <h2>
          {match.params.title}
      </h2>
    )
}

const Posts = (props) => {
  return (
    <div>
      <h1>포스트</h1>
      <Link to="/posts/react">React</Link>
      <Link to="/posts/redux">Redux</Link>
      <Link to="/posts/relay">relay</Link>
      <Route path="/posts/:title" component={Post} />
    </div>
  )
}

export default Posts
