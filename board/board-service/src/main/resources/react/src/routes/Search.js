import React from 'react'
import PropTypes from 'prop-types'

const Search = ({location}) => {
  return (
    <div>
      {new URLSearchParams(location.search).get('keyword')}
    </div>
  )
}

export default Search
