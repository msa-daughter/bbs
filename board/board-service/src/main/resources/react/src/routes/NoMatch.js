import React from 'react'
import PropTypes from 'prop-types'

const NoMatch = () => {
  return (
    <div>
      404 not found
    </div>
  )
}

export default NoMatch
