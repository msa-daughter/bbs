import React from 'react'
import PropTypes from 'prop-types'

const Home = ({history}) => {
  return (
    <div>
      Home
      <button onClick={()=>{history.push('/posts')}}>
        버튼
      </button>
    </div>
  )
}

export default Home
