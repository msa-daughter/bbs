import React from 'react'
import PropTypes from 'prop-types'
import { BrowserRouter as Router, Route, Switch } from 'react-router-dom';
//import List from './routes/List';
import List from './containers/List';
import Write from './containers/Write';
import Post from './containers/Post';

import NoMatch from './routes/NoMatch';
import Header from './components/Header';

const App = () => {
  return (
    <Router>
      <div>
        <Switch>
          <Route exact path={window.reactRoutePath} component={NoMatch}/>
          <Route exact path={`${window.reactRoutePath}:userid`} component={List}/>
          <Route exact path={`${window.reactRoutePath}:userid/write`} component={Write}/>
          <Route path={`${window.reactRoutePath}:userid/:postid`} component={Post}/>
          /* <Route exact path="/write" component={Write}/> */
          <Route component={NoMatch}/>
        </Switch>
      </div>
    </Router>
  )
}

export default App
