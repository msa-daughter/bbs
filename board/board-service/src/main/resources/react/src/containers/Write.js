import React, {Component} from 'react';
import PropTypes from 'prop-types';

import WriteHeader from '../components/write/WriteHeader'
import WritePost from '../components/write/WritePost'

import * as service from '../services/service';

export default class Write extends Component {

  constructor(props) {
    super(props);

    this.state = {
        fetching: false, // tells whether the request is waiting for response or not
        categories: []
    };
    let userId = props.match.params.userid;
    window.userId = userId;
    this.blogId = '';
    //let match = props.match;
  }

  componentDidMount() {
      try{
        this.fetchCategoryInfo(window.userId);
      }
      catch(e)
      {
        console.log(e);
      }
  }

  fetchCategoryInfo = async (userId) => {
      this.setState({
        fetching: true // requesting..
      });

      const response = await service.getCategoriesByUserId(userId);
      const {data} = response;

      console.log(data);

      this.blogId = data[0].blogId;

      this.setState({
        categories: data,
        fetching: false // done!
      });
  }

  handleCategoryAdd = (categoryName) => {
    try{
      if (categoryName == '')
      {
          document.getElementById('newCategoryName').focus();
          return false;
      }
      this.addCategoryInfo(categoryName);
    }
    catch(e)
    {
      console.log(e);
    }
  }

  addCategoryInfo = async (categoryName) => {

      const category = {};
      category.blogId = this.blogId;
      category.name = categoryName;

      const categories = await service.registerCategory(category);

      console.log(categories);

      this.fetchCategoryInfo(window.userId);
  }

  handleCategoryMod = (categoryId, categoryName, prevProps) => {
    try{
      console.log(prevProps);
      console.log(categoryName);

      if (prevProps.categoryName == categoryName)
      {
          return false;
      }

      if (categoryName == '')
      {
          document.getElementById('newCategoryName').focus();
          return false;
      }
      this.modCategoryInfo(categoryId, categoryName);
    }
    catch(e)
    {
      console.log(e);
    }
  }

  modCategoryInfo = async (categoryId, categoryName) => {

      const modCategory = {};
      modCategory.categoryId = categoryId;
      modCategory.nameValues = [];
      modCategory.nameValues.push({"name":"categoryName", "value":categoryName});

      const categories = await service.modifyCategory(modCategory);

      console.log(categories);

      this.fetchCategoryInfo(window.userId);

  }



  render() {
    return (
      <div>
      <WriteHeader/>
      <WritePost onAdd={this.handleCategoryAdd} onEdit={this.handleCategoryMod} categories={this.state.categories} userId={this.props.match.params.userid}/>
      </div>
    );
  }

}
