import React, {Component} from 'react';
import PropTypes from 'prop-types';

import Header from '../components/Header'
import Content from '../components/post/Content'


import * as service from '../services/service';

export default class Post extends Component {

  constructor(props) {
    super(props);

    this.state = {
        fetching: false,
        post: {},
        categories:[],
        blog: {
            blogName: null,
            createdDate: null,
            description: null,
            id: null,
            userId: null,
            userName: null
        }
    };
    this.userId = props.match.params.userid;
    this.postId = props.match.params.postid;
    window.postId = this.postId;
    window.userId = this.userId;
  }

  componentDidMount() {
      try{
         this.fetchPostInfo(this.userId, this.postId);
      }
      catch(e)
      {
        console.log(e);
      }
  }
  fetchPostInfo = async (userId, postId) => {

      this.setState({
        fetching: true // requesting..
      });

      const postInfo = await Promise.all([
         service.getCategoriesByUserId(userId),
         service.findPost(postId),
         service.findBlogByUserId(userId),
         service.findCommentByPostId(postId),
      ]);

      const categories = postInfo[0].data;
      const post = postInfo[1].data;
      const blog = postInfo[2].data;

      this.setState({
        categories: categories,
        post: post,
        blog: blog,
        fetching: false // done!
      });
  }


  render() {
    return (
      <div>
      <Header postHeader="true" title={this.state.blog.blogName} postTitle={this.state.post.title}/>
      <Content post={this.state.post} categories={this.state.categories}/>
      </div>
    );
  }

}
