import React, {Component} from 'react';
import PropTypes from 'prop-types';

import Header from '../components/Header'
import Lists from '../components/list/Lists';
import * as service from '../services/service';

export default class List extends Component {

  constructor(props) {
     super(props);

     const search = props.location.search;

     if (search)
     {
       console.log('if');
     }
     
     // initializes component state
     this.state = {
         fetching: false, // tells whether the request is waiting for response or not
         categories: [],
         blog: {
             blogName: null,
             createdDate: null,
             description: null,
             id: null,
             userId: null,
             userName: null
         },
         posts: [],
         query:'',
         queryType: 'd'
     };

     let userId = props.match.params.userid;
     window.userId = userId;
 }


  componentDidMount() {
      try{
        this.fetchPostInfo();
      }
      catch(e)
      {
        console.log(e);
      }
  }
  fetchPostInfo = async () => {

      const queryString = this.props.location.search;
      const userId = this.props.match.params.userid;
      const params = new URLSearchParams(queryString);
      const c = params.get('c');
      const q = params.get('q');
      const t = params.get('t');

      let type = 'd';
      let findPost = () => {};
      let param = this.props.match.params.userid;

      if (c != null) {type = 'c';}
      else if (q != null) {type = 'q';}
      else if (t != null) {type = 't';}
      else {type = 'd'}

      switch(type) {
         case "c": {
            findPost = service.findPostByCategoryId;
            param = c;
            break;
         }
         case "q": {
            findPost = service.findPostByTitle;
            param = q;
            break;
         }
         case "t": {
            findPost = service.findPostByTagName;
            param = t;
            break;
         }
         default: {
            findPost = service.findPostByUserId;
            param = userId;
            break;
         }
      }

      this.setState({
        fetching: true // requesting..
      });

      const blogInfo = await Promise.all([
         service.findBlogByUserId(userId),
         service.getCategoriesByUserId(userId),
         findPost(param)
      ]);

      const blog = blogInfo[0].data;
      const categories = blogInfo[1].data;
      const posts = blogInfo[2].data;

      this.setState({
        categories: categories,
        blog: blog,
        posts:posts,
        fetching: false // done!
      });
  }

  render() {
    return (
      <div>
      <Header title={this.state.blog.blogName}/>
      <Lists posts={this.state.posts} categories={this.state.categories}/>
      </div>
    );
  }

}
