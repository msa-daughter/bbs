import axios from 'axios';


export function findBlogByUserId(userId) {
  return axios.get('http://127.0.0.1:8080/board/api/s/blogs?userId=' + userId, {crossdomain: true});
}

export function getCategories(blogid) {
  return axios.get('http://127.0.0.1:8080/board/api/s/categories?blogId=' + blogid, {crossdomain: true});
}

export function getCategoriesByUserId(userId) {
  return axios.get('http://127.0.0.1:8080/board/api/s/categories/users/' + userId, {crossdomain: true});
}


export function findPostByUserId(userId) {
  return axios.get(`http://127.0.0.1:8080/board/api/s/posts/user/${userId}?offset=0&limit=10`);
}

export function findPostByCategoryId(categoryId) {
  return axios.get(`http://127.0.0.1:8080/board/api/s/posts/category/${categoryId}?offset=0&limit=10`);
}

export function findPostByTitle(keyword) {
  return axios.get(`http://127.0.0.1:8080/board/api/s/posts/title?title=${keyword}&offset=0&limit=10`);
}

export function findPostByTagName(tagName) {
  return axios.get(`http://127.0.0.1:8080/board/api/s/posts/tag?tagName=${tagName}&offset=0&limit=10`);
}


export function findPost(postId) {
  return axios.get(`http://127.0.0.1:8080/board/api/s/posts/${postId}`);
}


/*
export function getCategories(blogid) {
  return axios.get('http://127.0.0.1:8080/board/api/s/categories?blogId=' + blogid, {crossdomain: true})
  .then(function(response) {
    console.log('response is : ' + response.data);
    console.log(response.data);
  }).catch(function(error) {
    if (error.response) {
      console.log(error.response.headers);
    } else if (error.request) {
      console.log(error.request);
    } else {
      console.log(error.message);
    }
    console.log(error.config);
  });
}
*/
/*
{
  "categoryId": "string",
  "content": "string",
  "tagNames": [
    "string"
  ],
  "title": "string",
  "userId": "string"
}

*/

export function writePost(post) {
  console.log(JSON.stringify(post));
  return axios.post('http://127.0.0.1:8080/board/api/s/posts', post, {crossdomain: true});
}



/*
comment
{
  "content": "string",
  "postId": "string",
  "userId": "string",
  "userName": "string"
}
*/

export function findCommentByPostId(postId) {
  console.log(JSON.stringify(postId));
  return axios.get(`http://127.0.0.1:8080/board/api/s/comments?postId=${postId}`, {crossdomain: true});
}

export function registerComment(comment) {
  console.log(JSON.stringify(comment));
  return axios.post('http://127.0.0.1:8080/board/api/s/comments', comment, {crossdomain: true});
}


export function registerCategory(category) {
  return axios.post('http://127.0.0.1:8080/board/api/s/categories', category, {crossdomain: true});
}

export function modifyCategory(modCategory) {
  return axios.post('http://127.0.0.1:8080/board/api/s/categories/'+modCategory.categoryId, modCategory, {crossdomain: true});
}
