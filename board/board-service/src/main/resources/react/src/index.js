import React from 'react';
import ReactDOM from 'react-dom';
import './index.css';
import './Style.css';
import './Genericon.css';
import App from './App';

import { createStore } from 'redux';
import { Provider } from 'react-redux';

import reducers from './reducers';

const store = createStore(reducers);

window.reactRoutePath = '/';

ReactDOM.render(
  <Provider store={store}>
    <App />
  </Provider>
, document.getElementById('page'));
