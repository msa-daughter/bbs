import React, {Component} from 'react';


class Title extends Component {

  constructor(props) {
    super(props);
    this.prevProps = null;
  }

  componentDidUpdate(prevProps, prevState){
      document.getElementById('category').value = '';
      this.prevProps = prevProps;
  }

  render()
  {
    const mapToCompoenets = (data) => {
      return data.map((category, i ) => {
          return (<option value={category.id} key={i}>{category.categoryName}</option>);
      });
    };

    const selectStyle = {
      "width":"140px",
      "padding": "0.9%",
      "marginRight":"8px"
    }

    const textStyle = {
      "width":"80%",
      "boxSizing":
      "border-box"
    }

    return(
      <div>
         <select id="category" style={selectStyle}>
          <option value="">카테고리 선택</option>
          {mapToCompoenets(this.props.categories)}
         </select>
         <input id="title" type="text" style={textStyle} placeholder="제목을 여기에 입력하세요."/>
      </div>
    )
  }
}

export default Title
