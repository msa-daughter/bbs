import React, {Component} from 'react';
import PropTypes from 'prop-types'
import Comment from './comment'
import Tag from './Tag'
import showdown from 'showdown'
import renderHTML from 'react-render-html';

class PostContent extends Component
{
  constructor(props) {
    super(props);
  }

  // 컴포넌트가 DOM 위에 만들어지기 전에 실행됩니다.
  componentWillMount(){
    console.log("componentWillMount");
  }

  // 컴포넌트가 만들어지고 첫 렌더링을 다 마친 후 실행되는 메소드입니다.
  componentDidMount(){
      console.log("componentDidMount");
  }

  // 컴포넌트가 prop 을 새로 받았을 때 실행됩니다.
  // prop 에 따라 state 를 업데이트 해야 할 때 사용하면 유용합니다.
  componentWillReceiveProps(nextProps){
    console.log("componentWillReceiveProps: " + JSON.stringify(nextProps));
  }

  // prop 혹은 state 가 변경 되었을 때, 리렌더링을 할지 말지 정하는 메소드
  shouldComponentUpdate(nextProps, nextState){
      console.log("shouldComponentUpdate: " + JSON.stringify(nextProps) + " " + JSON.stringify(nextState));
      return true;
  }

  // 컴포넌트가 업데이트 되기 전에 실행됩니다.
  componentWillUpdate(nextProps, nextState){
    console.log("componentWillUpdate: " + JSON.stringify(nextProps) + " " + JSON.stringify(nextState));
  }

  // 컴포넌트가 DOM 에서 사라진 후 실행되는 메소드입니다.
  componentWillUnmount(){
    console.log("componentWillUnmount");
  }

  render()
  {
    const tagComponents = (data) => {

      // console.log(data);
      if(data){
        const tagComponents = data.map((tagName, i ) => {
            return (<Tag key={i} tagName={tagName}/>);
        });

        if (tagComponents.length > 0)
        {
          const compArray = [];
          compArray.push(<span>태그: </span>);
          return compArray.concat(tagComponents);
        }
      }
    }

    const categoryName = (category) => {
      console.log(category);
      if (category){
        return category.categoryName;
      }
    }

    const commentCount = () => {

    }

    const mdToHtml = (text) => {

      if (text)
      {
        console.log(text);
        const converter = new showdown.Converter();
        const html = converter.makeHtml(text);
        console.log(html);

        return renderHTML(html);
      }
    }

    return (
      <div id="primary">
        <div id="content" className="clearfix">
            <article className={["post", "type-post", "status-publish", "format-standard", "hentry", "category-uiux-"].join(' ')}>
            <div className={["entry-content", "clearfix"].join(' ')}>
            {mdToHtml(this.props.post.content)}
            </div>

            <div className="tags">
              {tagComponents(this.props.post.tagNames)}
            </div>

            <footer className={["entry-meta-bar", "clearfix"].join(' ')}>
  						<div className={["entry-meta", "clearfix"].join(' ')}>
  							<span className={["by-author", "author", "vcard"].join(' ')}><a className={["url", "fn", "n"].join(' ')} href="#">저자</a></span>
  							<span className="date"><a href="#" title="2:49 오후" rel="bookmark"><time className={["entry-date", "published"].join(' ')} dateTime="2017-11-30T14:49:47+00:00">2017년 11월 30일</time></a></span>
  							<span className="category"><a rel="category">{categoryName(this.props.post.category)}</a></span>
  							<span className="comments"><a>댓글 없음</a></span>
  						</div>
  					</footer>
            </article>
        </div>
        <Comment postId={this.props.post.id} title={this.props.post.title}/>
      </div>
    );
  }

}

PostContent.defaultProps = {
  post: {
    'category':{'categoryName':'카테고리없음'},
    'tags':[{'tagName':'111'},{'tagName':'222'}]
  }
};

export default PostContent;
