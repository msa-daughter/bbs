import React from 'react'
import PropTypes from 'prop-types'
import { Link } from 'react-router-dom';

const Article = (props) => {
  return (
    <article className={["post", "type-post", "status-publish", "format-standard", "hentry", "category-uiux-"].join(' ')}>

      <header className="entry-header">
    		<h2 className="entry-title">
    			<Link to={window.reactRoutePath + window.userId +'/'+props.post.id}>{props.post.title}</Link>
    		</h2>
    	</header>

      <div className={["entry-content", "clearfix"].join(' ')}>
        <p>{props.post.content}</p>
      </div>

      <footer className={["entry-meta-bar", "clearfix"].join(' ')}>
    		<div className={["entry-meta", "clearfix"].join(' ')}>
    			<span className={["by-author", "author", "vcard"].join(' ')}>
            <a className={["url", "fn", "n"].join(' ')} href="#">작성자</a>
          </span>
    			<span className="date"><a href="#" title="2:49 오후" rel="bookmark">
            <time className={["entry-date", "published"].join(' ')} dateTime="2017-11-30T14:49:47+00:00">{props.post.createdDate}</time></a>
          </span>
    			<span className="category">
            <a href={window.reactRoutePath + window.userId+'?c='+props.post.categoryId} rel="category">{props.post.category.categoryName}</a>
          </span>
    			<span className="comments">
            <a href="#respond">댓글 없음</a>
          </span>
    			<span className="read-more-link">
            <Link to={window.reactRoutePath + window.userId +'/'+props.post.id} className="read-more">더 읽기</Link>
          </span>
    		</div>
    	</footer>

    </article>
  )
}

export default Article
