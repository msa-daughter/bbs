import React, {Component} from 'react';
import PropTypes from 'prop-types'

class TagWidget extends Component {


  constructor(props) {
    super(props);

    this.state = {
        tags: [],
    };
  }

  handleKeyPress = (e) => {

    if (e.charCode == 13)
    {
      if (document.getElementById('inputTags').value.length > 0)
      {
        this.addTags();
      }
    }

  }

  addTags = (e) => {

    const tagsValue = document.getElementById('inputTags').value;

    if (tagsValue == '')
    {
      document.getElementById('inputTags').focus();
      return false;
    }


    const array = tagsValue.split(',');

    const allTagList = this.state.tags.concat(array).filter(entry => entry.trim().length > 0); // merge. trim
    const uniqArray = Array.from(new Set(allTagList)); // distict
    this.setState({
      tags: uniqArray
    });


    document.getElementById('inputTags').value = '';
  }

  render(){

    const style = {
      "float": "left",
      "font-size": "13px",
      "line-height": "1.8em",
      "margin-right": "25px",
      "cursor": "default",
      "max-width": "100%",
      "overflow": "hidden",
      "text-overflow": "ellipsis",
    }

    const tagItems = (tags) => {

      let tagComponents = tags.map((tag, i ) => {
          return (<span style={style}>
            <button type="button" id="post_tag-check-num-1" className="remove-icon" style={{"marginBottom":"5px", "marginRight":"3px"}}>
            </button>{tag}
          </span>);
      });

      tagComponents.push(<input id="registerTags" type="hidden" value={this.state.tags.join(',')}/>)
      console.log(tagComponents);
      return tagComponents;

    }

    return(
      <aside id="categories-7" className={["widget ", "widget_categories"].join(' ')}>
        <h3 className="widget-title"><span>태그</span></h3>
        <div>
          <p>
            <input id="inputTags" style={{"width": "60%", "marginBottom":"0"}} type="text" onKeyPress={this.handleKeyPress}/> <button style={{"marginBottom":"0"}} onClick={this.addTags}>추가</button>
          </p>
          <p className="howto" id="new-tag-post_tag-desc">각 태그를 쉼표로 분리하세요.</p>
          <div className="tagchecklist">
            {tagItems(this.state.tags)}
          </div>
        </div>
      </aside>
    );
  }
}
export default TagWidget
