import React, {Component} from 'react';
import PropTypes from 'prop-types'
import * as service from '../../../services/service';

export default class OpenWidget extends Component {

  constructor(props) {
    super(props);

    this.state = {
        fetching: false
    };
  }

 fetchPostInfo = async (post) => {

      this.setState({
        fetching: true // requesting..
      });

      console.log(JSON.stringify(post));
      const result = await service.writePost(post);
      console.log(result);

      this.setState({
        fetching: false // done!
      });
  }

  validationForm = (postRef) =>
  {
    let invaildFlag = false;
    if (postRef.categoryId.value == '')
    {
      alert('카테고리를 선택해 주십시오');
      postRef.categoryId.focus();
      invaildFlag = true;
    }
    else if (postRef.title.value == '')
    {
      alert('제목을 입력해 주십시오');
      postRef.title.focus();
      invaildFlag = true;
    }
    else if (postRef.content.value == '')
    {
      alert('내용을 입력해 주십시오');
      postRef.content.focus();
      invaildFlag = true;
    }

    return invaildFlag;
  }

  openPost = () =>
  {
    const post = {};
    const postRef = {};
    const tags = document.getElementById('registerTags').value;


    postRef.categoryId = document.getElementById('category');
    postRef.content = document.getElementById('ta1');
    postRef.title = document.getElementById('title');

    //validation
    if (this.validationForm(postRef))
    {
      return false;
    }

    post.userId = window.userId
    post.categoryId = postRef.categoryId.value;
    post.content = postRef.content.value;
    if (tags != '')
    {
      post.tagNames = tags.split(',');
    }
    else {
      post.tagNames = [];
    }

    post.title = postRef.title.value;


    try{
      this.fetchPostInfo(post);
    }
    catch(e)
    {
      console.log(e);
    }

    alert('POST가 등록 되었습니다.');
    document.location.href=window.reactRoutePath+post.userId;
    /*
      "categoryId": "string",
      "content": "string",
      "tagNames": [
        "string"
      ],
      "title": "string",
      "userId": "string"
    */


  }

  render() {
    return (
      <aside id="categories-7" className={["widget ", "widget_categories"].join(' ')}>
  			<h3 className="widget-title"><span>공개하기</span></h3>
  			<ul>
  				<li className={["cat-item ", "cat-item-5"].join(' ')}> <button onClick={this.openPost}>공개하기</button>
  				</li>
  			</ul>
  		</aside>
    );
  }
}
