import React from 'react'
import PropTypes from 'prop-types'

const PostHeader = (props) => {
  return (
    <div className={["header-post-title-container ", "clearfix"].join(' ')}>
			<div className="inner-wrap">
				<div className="post-title-wrapper">
					<h1 className="header-post-title-class">{props.postTitle}</h1>
				</div>
			</div>
		</div>
  )
}

export default PostHeader
