import React from 'react'
import PropTypes from 'prop-types'

const Tag = (props) => {
  return (
    <a onClick={()=>{document.location.href=window.reactRoutePath + window.userId +'?t=' + props.tagName}} rel="tag" style={{'marginRight':'10px'}}>{props.tagName}</a>
  )
}

export default Tag
