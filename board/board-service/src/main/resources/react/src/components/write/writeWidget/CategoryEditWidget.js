import React, {Component} from 'react';
import * as service from '../../../services/service';


class CategoryEditWidget extends Component {

  constructor(props) {
    super(props);

    this.state = {
        fetching: false,
        isEdit: false,
        isAdd: false,
        editCategoryId: '',
    };

    this.blogId = '';
  }

  componentDidMount() {
      try{
         this.categoriesInit();
      }
      catch(e)
      {
        console.log(e);
      }
  }

  categoriesInit() {
    try{
       this.fetchCategoryInfo(window.userId);
    }
    catch(e)
    {
      console.log(e);
    }
  }

  // 안씀
  handleCategoryAdd = (categoryName) => {
    try{
      if (categoryName == '')
      {
          alert('카테고리 이름을 입력해 주세요.');
          return false;
      }
      this.addCategoryInfo(categoryName);
    }
    catch(e)
    {
      console.log(e);
    }
  }

  addCategoryInfo = async (categoryName) => {

      const category = {};
      category.blogId = this.blogId;
      category.name = categoryName;

      const categories = await service.registerCategory(category);

      console.log(categories);

      this.categoriesInit();

  }



  handleEdit = (categoryId, flag) => {
    this.setState({
        isEdit:flag,
        isAdd:false,
        categoryId:categoryId
    })
  }

  handleAdd = (flag) => {
    this.setState({
        isEdit: false,
        isAdd:flag
    })
  }

  modCategoryInfo = async (categoryId, categoryName) => {

      const modCategory = {};
      modCategory.categoryId = categoryId;
      modCategory.nameValues = [];
      modCategory.nameValues.push({"name":"categoryName", "value":categoryName});

      const categories = await service.modifyCategory(modCategory);

      console.log(categories);

      this.categoriesInit();

  }


  fetchCategoryInfo = async (userId) => {

      this.setState({
        fetching: true // requesting..
      });

      const categories = await service.getCategoriesByUserId(userId);

      console.log(categories);

      this.setState({
        isEdit: false,
        categories: categories.data,
        fetching: false // done!
      });

      this.blogId = categories.data[0].blogId;
  }

  render() {

    const mapToComponents = (data, editCategoryId) => {

      if (data)
      {
        const categoryItems =  data.map((category, i ) => {

            if (category.id == this.state.categoryId)
            {
              return (<CategoryMod key={i} isEdit={this.state.isEdit} onEdit={this.handleEdit} onClick={this.props.onEdit} categoryId={category.id} categoryName={category.categoryName} />);
            }
            else
            {
              return (<CategoryMod key={i} isEdit={false} onEdit={this.handleEdit} onClick={this.props.onEdit} categoryId={category.id} categoryName={category.categoryName} />);
            }
        });

        categoryItems.push(<CategoryAdd isAdd={this.state.isAdd} onAdd={this.handleAdd} onClick={this.props.onAdd}/>)

        return categoryItems;
      }
    };

    return(
      <aside className={["widget ", "widget_categories"].join(' ')}>
        <h3 className="widget-title"><span>카테고리</span></h3>
        <ul>
          {mapToComponents(this.props.categories, this.state.editCategoryId)}
        </ul>
      </aside>
    );
  }

}

class CategoryMod extends Component {

  constructor(props) {
    super(props);

    this.prevProps = null;
  }


  componentDidUpdate(prevProps, prevState){

    if (this.props.isEdit)
    {
      const categoryId = this.props.categoryId;
      const categoryName = this.props.categoryName;

      document.getElementById(categoryId).value = categoryName;
      this.prevProps = prevProps;
    }
  }

  render() {

    const edit = (<li className="cat-item">
                    <input id={this.props.categoryId} type='text' style={{"marginBottom":"0", "width":"30%"}}/>
                    <button onClick={() => {this.props.onClick(this.props.categoryId, document.getElementById(this.props.categoryId).value, this.prevProps); this.props.onEdit(false);}} style={{"marginBottom":"0", "marginLeft":"5px", "paddingLeft":"5px", "paddingRight":"5px"}}>변경</button>
                    <button onClick={() => {this.props.onEdit(false)}} style={{"marginBottom":"0", "marginLeft":"5px", "paddingLeft":"5px", "paddingRight":"5px"}}>취소</button>
                  </li>);
    const info = (<li className="cat-item">{this.props.categoryName} <a onClick={() => {this.props.onEdit(this.props.categoryId, true)}}>수정</a></li>);

    return(
      <div>
        {this.props.isEdit? edit : info}
      </div>
    );
  }

}

class CategoryAdd extends Component {

  constructor(props) {
    super(props);
  }

  render() {

    const edit = (<li className="cat-item">
                    <input id="newCategoryName" type='text' style={{"marginBottom":"0", "width":"30%"}}/>
                    <button onClick={() => {this.props.onClick(document.getElementById('newCategoryName').value)}} style={{"marginBottom":"0", "marginLeft":"5px", "paddingLeft":"5px", "paddingRight":"5px"}}>추가</button>
                    <button onClick={() => {this.props.onAdd(false)}} style={{"marginBottom":"0", "marginLeft":"5px", "paddingLeft":"5px", "paddingRight":"5px"}}>취소</button>
                  </li>);
    const info = (<li className="cat-item"> <a onClick={() => {this.props.onAdd(true)}}> + 카테고리 추가 </a></li>);

    return(
      <div>
        {this.props.isAdd? edit : info}
      </div>
    );
  }

}

export default CategoryEditWidget
