import React from 'react'
import PropTypes from 'prop-types'

import CommentItem from './CommentItem'

const CommentList = (props) => {

  const commentItemComponents = (comments) => {
    if(comments){
      return comments.map((item, i ) => {
          return (<CommentItem key={i} userName={item.userName} createdDate={item.createdDate} content={item.content}/>);
      });
    }
  }

  const commnetCount = (comments) => {

    if(comments){
      return comments.length;
    }

  }

  return (
    <div>
    <h3 className="comments-title">
			“<span>{props.title}</span>”에 대한 {commnetCount(props.comments)}개의 코멘트
    </h3>
    <ul className="comment-list">
      {commentItemComponents(props.comments)}
		</ul>
    </div>
  )
}

export default CommentList
