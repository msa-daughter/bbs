import React from 'react'
import PropTypes from 'prop-types'

import OpenWidget from './OpenWidget'
import TagWidget from './TagWidget'
import PreviewWidget from './PreviewWidget'
import CategoryEditWidget from './CategoryEditWidget'

const Widgets = (props) => {
  return (
    <div id="secondary">
      <OpenWidget/>
      <CategoryEditWidget onAdd={props.onAdd} onEdit={props.onEdit} categories={props.categories}/>
      <TagWidget/>
    </div>
  )
}

export default Widgets
