import React, {Component} from 'react';
import PropTypes from 'prop-types'

import CommentList from './CommentList'
import CommentWrite from './CommentWrite'

import * as service from '../../../services/service';


export default class Comment extends Component {

  constructor(props) {
     super(props);
     console.log (props);

     this.state = {
       fetching: true,
       comments: []
     };
  }

  componentDidMount() {
      console.log(window.postId);
      try{
        this.fetchCommnetInfo(window.postId);
      }
      catch(e)
      {
        console.log(e);
      }
  }

  registerCommnet  = async (comment) => {

    this.setState({
      fetching: true // requesting..
    });

    const postInfo = await
       service.registerComment(comment);

    const comments = await
       service.findCommentByPostId(window.postId);

    this.setState({
      fetching: false, // done!
      comments: comments.data
    });

  }

  fetchCommnetInfo  = async (postId) => {

    this.setState({
      fetching: true // requesting..
    });

    const comments = await
       service.findCommentByPostId(postId);


    console.log(comments);

    this.setState({
      fetching: false, // done!
      comments: comments.data
    });

  }


  onRegister = (e) => {

    let comment = {};
    comment.content = document.getElementById('comment').value;
    comment.postId = window.postId;
    comment.userId = 'spectra';
    comment.userName = '스펙트라';

    if (comment.content == '')
    {
      document.getElementById('comment').focus();
      return false;
    }

    const result = this.registerCommnet(comment);

    console.log(result);

    // form 초기화
    document.getElementById('comment').value = '';


  }

  fetchPostInfo = async (comment) => {

      this.setState({
        fetching: true // requesting..
      });

      const postInfo = await
         service.registerComment(comment);
      console.log(postInfo);

      this.setState({
        fetching: false // done!
      });
  }

  render() {
    return (
      <div id="comments" className="comments-area">
      <CommentList comments={this.state.comments} title={this.props.title}/>
      <CommentWrite onRegister={this.onRegister}/>
      </div>
    );
  }

}
