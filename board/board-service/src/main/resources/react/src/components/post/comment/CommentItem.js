import React from 'react'
import PropTypes from 'prop-types'
import renderHTML from 'react-render-html';
import avatar from './avatar.png'

const CommentItem = (props) => {

  const textToHtml = (content) =>
  {
    content = content.replace(/(?:\r\n|\r|\n)/g, '<br />');
    return renderHTML(content);
  }

  return (
    <li className={["comment ", "byuser","odd", "alt", "thread-odd", "thread-alt", "depth-1"].join(' ')} id="li-comment-14">
      <article id="comment-14" className="comment">
        <header className={["comment-meta", "comment-author", "vcard"].join(' ')}>
          <img alt="" src={avatar} className={["avatar", "avatar-74", "photo"].join(' ')} height="74" width="74" />
          <div className="comment-author-link">{props.userName}</div>
          <div className="comment-date-time">{props.createdDate}</div>
        </header>
        <section className={["comment-content", "comment"].join(' ')}>
          <p>{props.content}</p>
        </section>
      </article>
    </li>
  )
}

export default CommentItem
