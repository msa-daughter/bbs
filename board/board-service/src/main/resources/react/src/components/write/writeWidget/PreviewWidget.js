import React, {Component} from 'react';
import PropTypes from 'prop-types'
import './preview.css'

class PreviewWidget extends Component {


  constructor(props) {
    super(props);

    this.state = {
        isPreview: false,
    };
  }

  handleKeyPress = (e) => {

    if (e.charCode == 13)
    {
      if (document.getElementById('inputTags').value.length > 0)
      {
        this.addTags();
      }
    }

  }

  render(){

    return(
      <aside id="categories-7" className={["widget ", "widget_categories"].join(' ')}>
        <h3 className="widget-title"><span>미리보기</span></h3>
        <div class="checkbox switcher">
          <label for="test2">
            <input type="checkbox" id="test2" value="" checked />
            <span><small></small></span>
            <small>Music</small>
          </label>
        </div>
      </aside>
    );
  }
}
export default PreviewWidget
