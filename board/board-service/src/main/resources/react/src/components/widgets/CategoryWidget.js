import React from 'react'
import PropTypes from 'prop-types'
import { Link } from 'react-router-dom';

const propTypes = {
  categories: PropTypes.array
};

const defaultProps = {
  categories: [{"id":'',"categoryName":"교육자료", "useCount":28},
              {"id":'',"categoryName":"기술자료", "useCount":34},
              {"id":'',"categoryName":"스터디그룹", "useCount":20},
              {"id":'',"categoryName":"임시자료", "useCount":1},
              {"id":'',"categoryName":"제품자료", "useCount":4}]
};

const CategoryWidget = (props) => {

  const { categories } = props;

  const mapToCompoenets = (data) => {
    data.sort(); //asc

    return data.map((category, i ) => {
        return (<li key={i} className="cat-item">
                  <a onClick={()=>{document.location.href=window.reactRoutePath + window.userId +'?c=' + category.id}}>{category.categoryName}</a> ({category.useCount})</li>);
    });
  };

  return (
    <aside className={["widget ", "widget_categories"].join(' ')}>
      <h3 className="widget-title"><span>카테고리</span></h3>
      <ul>
        {mapToCompoenets(categories)}
      </ul>
    </aside>
  )
}

CategoryWidget.propTypes = propTypes;
CategoryWidget.defaultProps = defaultProps;
export default CategoryWidget
