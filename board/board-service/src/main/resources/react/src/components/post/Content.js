import React from 'react'
import PropTypes from 'prop-types'
import PostContent from './PostContent'
import Widgets from '../widgets'

const Content = (props) => {
  return (
    <div id="main" className="clearfix">
      <div className="inner-wrap">
        <PostContent post={props.post}/>
        <Widgets categories={props.categories}/>
      </div>
    </div>
  )
}

export default Content
