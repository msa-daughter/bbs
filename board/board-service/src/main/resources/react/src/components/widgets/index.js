import React from 'react'
import PropTypes from 'prop-types'

import SearchWidget from './SearchWidget'
import CategoryWidget from './CategoryWidget'

const Widgets = (props) => {
  return (
    <div id="secondary">
      <SearchWidget/>
      <CategoryWidget categories={props.categories}/>
    </div>
  )
}

export default Widgets
