import React from 'react'
import PropTypes from 'prop-types'
import { Link } from 'react-router-dom';
import banner from './banner.jpg'
import PostHeader from './PostHeader'

const Header = (props) => {


  const linkPath = (addPath) => {

    let path = window.reactRoutePath + window.userId;
    if (addPath)
    {
       path += addPath;
    }
    return path
  }

  return (
    <header id="masthead" className={["site-header", "clearfix"].join(' ')}>
      <div id="wp-custom-header" className="wp-custom-header">
      <img src={banner} className="header-image"/></div>
        <div id="header-text-nav-container">
  				<div className="inner-wrap">
  					<div id="header-text-nav-wrap" className="clearfix">
  						<div id="header-left-section">
  							<div id="header-text">
  								<h1 id="site-title">
  								<a onClick={()=>{document.location.href=linkPath()}}>{props.title}</a>
  							</h1>
  							</div>
  						</div>
  						{ /* #header-left-section */ }
  						<div id="header-right-section">
  							<nav id="site-navigation" className="main-navigation">
  								<h3 className="menu-toggle">메뉴</h3>
  								<div className="menu-menu-1-container">
  									<ul id="menu-menu-1" className={["menu", "nav-menu"].join(' ')}>
  										<li id="menu-item-143" className={["menu-item", " current-menu-item", "current-menu-item", "current_page_item", "menu-item-home"].join(' ')}><a onClick={()=>{document.location.href=linkPath()}}>홈</a></li>
                      <li id="menu-item-143" className={["menu-item"].join(' ')}><Link to={linkPath('/write')}>글쓰기</Link></li>
  									</ul>
  								</div>
  							</nav>
  						</div>
  						{ /* #header-right-section */ }

  					</div>
  					{ /* #header-text-nav-wrap */ }
  				</div>
  			</div>
				{ /* #header-text-nav-container */ }

        {props.postHeader ? <PostHeader postTitle={props.postTitle}/> : null}


    </header>
  )
}

export default Header
