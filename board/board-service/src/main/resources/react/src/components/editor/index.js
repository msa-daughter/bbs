import React, { Component } from 'react';
import PropTypes from 'prop-types'
import ReactMde, { ReactMdeCommands } from 'react-mde';
import Title from './Title';

import './react-mde.css';
import 'react-mde/lib/styles/css/react-mde-toolbar.css';
import 'react-mde/lib/styles/css/react-mde-textarea.css';
import 'react-mde/lib/styles/css/react-mde-preview.css';
import 'font-awesome/css/font-awesome.min.css';


export default class Editor extends Component {

  constructor(props) {
      super(props);
      this.state = {
          reactMdeValue: {text: '', selection: null},
      };
  }

  handleValueChange = (value) => {
      this.setState({reactMdeValue: value});
  }

  render() {
         return (
           <div id="primary">
       			<div id="content" className="clearfix">
               <Title categories={this.props.categories}/>
               <div className="container">
                     <ReactMde
                         textAreaProps={{
                             id: 'ta1',
                             name: 'ta1',
                         }}
                         value={this.state.reactMdeValue}
                         onChange={this.handleValueChange}
                         commands={ReactMdeCommands.getDefaultCommands()}

                     />
               </div>
       			</div>
       		</div>
         );
  }

}
