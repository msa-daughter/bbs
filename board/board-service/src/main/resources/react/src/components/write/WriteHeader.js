import React from 'react'
import PropTypes from 'prop-types'
import { Link } from 'react-router-dom';
import banner from '../banner.jpg'

const WriteHeader = () => {

  var h2Style = {
    padding:0
  }

  return (
    <header id="masthead" className={["site-header", "clearfix"].join(' ')}>
			<div id="wp-custom-header" className="wp-custom-header">
      <img src={banner} className="header-image"/></div>
			<div id="header-text-nav-container">
				<div className="inner-wrap">
					<div id="header-text-nav-wrap" className="clearfix">
            <div id="header-left-section">
              <div id="header-text">
                <h1 id="site-title">
                새 글 쓰기
                </h1>
              </div>
            </div>
            <div id="header-right-section">
              <nav id="site-navigation" className="main-navigation">
                <h3 className="menu-toggle">메뉴</h3>
                <div className="menu-menu-1-container">
                  <ul id="menu-menu-1" className={["menu", "nav-menu"].join(' ')}>
                    <li id="menu-item-143" className={["menu-item", " current-menu-item", "current-menu-item", "current_page_item", "menu-item-home"].join(' ')}><Link to={window.reactRoutePath + window.userId}>홈</Link></li>
                  </ul>
                </div>
              </nav>
            </div>
					</div>
				</div>
			</div>
		</header>
  )
}

export default WriteHeader
