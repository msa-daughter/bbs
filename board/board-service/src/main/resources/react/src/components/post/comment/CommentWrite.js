import React from 'react'
import PropTypes from 'prop-types'

const CommentWrite = (props) => {
  return (
			<div id="respond" className="comment-respond">
				<h3 id="reply-title" className="comment-reply-title">답글 남기기 <small><a rel="nofollow" id="cancel-comment-reply-link" href="/wordpress/?p=3344#respond" style={{"display":"none"}}>응답 취소</a></small></h3>
				<form action="#" method="post" id="commentform" className="comment-form" noValidate="">
					<p className="comment-form-comment"><label htmlFor="comment">댓글</label>
            <textarea id="comment" name="comment" cols="45" rows="8" maxLength="65525" aria-required="true" required="required"></textarea></p>
						<p className="form-submit">
              <input name="submit" type="button" id="submit" onClick={props.onRegister} className="button" value="댓글 달기"/>
              <input type="hidden" name="comment_post_ID" value="3344" id="comment_post_ID"/>
  						<input type="hidden" name="comment_parent" id="comment_parent" value="0"/>
						</p>
				</form>
			</div>
  )
}

export default CommentWrite
