import React, {Component} from 'react';
import PropTypes from 'prop-types'
import Editor from '../editor'
import WriteWidgets from './writeWidget'


class WritePost extends Component
{
  constructor(props) {
     super(props);
  }

  render()
  {
    const style = {
      paddingTop: 20,
      paddingRight: 0,
      paddingBottom: 10,
      paddingLeft: 0
    }

    return(
      <div id="main" style={style} className="clearfix">
        <div className="inner-wrap">
          <Editor categories={this.props.categories}/>
          <WriteWidgets onAdd={this.props.onAdd} onEdit={this.props.onEdit} categories={this.props.categories}/>
        </div>
      </div>
    )
  }
}

export default WritePost
