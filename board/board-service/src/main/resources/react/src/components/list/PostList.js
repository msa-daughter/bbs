import React from 'react'
import PropTypes from 'prop-types'

import Article from './Article'

const PostList = (props) => {

  const mapToCompoenets = (data) => {
    return data.map((post, i ) => {
        return (<Article key={i} post={post}/>);
    });
  };

  return (
    <div id="primary">
      <div id="content" className="clearfix">
          {mapToCompoenets(props.posts)}
      </div>
    </div>
  )
}

export default PostList
