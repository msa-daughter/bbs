import React from 'react'
import PropTypes from 'prop-types'

import PostList from './PostList'
import Widgets from '../widgets'

const Lists = (props) => {
  return (
    <div id="main" className="clearfix">
      <div className="inner-wrap">
        <PostList posts={props.posts}/>
        <Widgets categories={props.categories}/>
      </div>
    </div>
  )
}

export default Lists
