import React from 'react'
import PropTypes from 'prop-types'

const SearchWidget = () => {
  return (
    <aside className={["widget ", "widget_search"].join(' ')}>
      <form action={window.reactRoutePath + window.userId} className={["search-form ", "searchform", "clearfix"].join(' ')}>
        <div className="search-wrap">
          <input type="text" placeholder="검색" className={["s", "field"].join(' ')} name="q" />
          <button className="search-icon" type="submit"></button>
        </div>
      </form>
    </aside>
  )
}

export default SearchWidget
