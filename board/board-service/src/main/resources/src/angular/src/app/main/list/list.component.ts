import {Component, OnInit} from '@angular/core';
import {CommonService} from '../common.service';
import {Http} from '@angular/http';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/mergeMap';
import 'rxjs/add/operator/debounceTime';
import {ActivatedRoute, Router} from '@angular/router';
import * as moment from 'moment';
import {FormControl} from '@angular/forms';

@Component({
  selector: 'app-list',
  templateUrl: './list.component.html',
  styleUrls: ['./list.component.css']
})
export class ListComponent implements OnInit {

  userName: string;
  blogName: string;
  blogId: string;
  offset: number;
  limit: number;
  posts: {}[];
  keywordInput: FormControl = new FormControl('');
  constructor(
    private commonService: CommonService,
    private http: Http,
    private router: Router,
    private route: ActivatedRoute) {
    this.userName = this.commonService.userName;
    this.blogId = this.route.snapshot.paramMap.get('blogId');
    this.offset = 0;
    this.limit = 10;
  }

  ngOnInit() {
    const getBlogUrl = `http://localhost:8080/board/api/s/blogs/${this.blogId}`;
    this.http.get(getBlogUrl).subscribe((data) => {
      this.blogName = data.json().blogName;
    });
    this.initPosts();
    this.keywordInput.valueChanges.debounceTime(500).subscribe((text) => {
      this.searchKeyword(text);
    });
  }
  initPosts() {
    const getPostsUrl = `http://localhost:8080/board/api/s/posts/blog/${this.blogId}?offset=${this.offset}&limit=${this.limit}`;
    this.http.get(getPostsUrl).subscribe((data) => {
      this.posts = data.json();
    });
  }
  getDate(date: string) {
    return moment(date, 'YYYYMMddHHmmss').toDate();
  }
  clickPost($event: Event, postId: string) {
    $event.preventDefault();
    this.router.navigate([`/post/${postId}`]);
  }
  clickCategory(categoryId) {
    this.offset = 0;
    const getPostsUrl = `http://localhost:8080/board/api/s/posts/category/${categoryId}?offset=${this.offset}&limit=${this.limit}`;
    this.http.get(getPostsUrl).subscribe((data) => {
      this.posts = data.json();
    });
  }
  searchKeyword(keyword) {
    if (!keyword || keyword === '') {
      this.initPosts();
    } else {
      const getPostsUrl = `http://localhost:8080/board/api/s/posts/title?title=${keyword}&offset=${this.offset}&limit=${this.limit}`;
      this.http.get(getPostsUrl).subscribe((data) => {
        if (data.json().length > 0) {
          this.posts = data.json();
        }
      });
    }
  }
}
