import {Component, EventEmitter, Input, OnInit, Output} from '@angular/core';
import {Http} from '@angular/http';

@Component({
  selector: 'app-comment',
  templateUrl: './comment.component.html',
  styleUrls: ['./comment.component.css']
})
export class CommentComponent implements OnInit {

  @Input() postId: string;
  @Output() onClick: EventEmitter<void>;
  commentsCount: number;
  constructor(
    private http: Http
  ) { }

  ngOnInit() {
    this.commentsCount = 0;
    this.loadComment();
  }
  loadComment() {
    const commentUrl = `http://localhost:8080/board/api/s/comments?postId=${this.postId}`;
    this.http.get(commentUrl).map(data => data.json()).subscribe(data => {
      this.commentsCount = data.length;
    });
  }
}
