import {Component, OnInit} from '@angular/core';
import {Http, Headers} from '@angular/http';
import {CommonService} from './main/common.service';
import {Router} from '@angular/router';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent implements OnInit {
  blogId: string;
  blogName: string;
  constructor(private commonService: CommonService, private router: Router) {}

  ngOnInit() {
    this.commonService.blogData.subscribe((data) => {
      this.blogId = data.id;
      this.blogName = data.blogName;
      this.router.navigate([`/list/${data.id}`]);
    });
  }
}
