import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import {ListComponent} from './main/list/list.component';
import {PostComponent} from './main/post/post.component';
import {WriteComponent} from './main/write/write.component';

const routes: Routes = [
  {path: 'list/:blogId', component: ListComponent},
  {path: 'post/:postId', component: PostComponent},
  {path: 'write/:blogId', component: WriteComponent}
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class RoutingModule { }
