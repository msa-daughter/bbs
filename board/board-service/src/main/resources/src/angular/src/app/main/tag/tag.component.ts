import {Component, EventEmitter, Input, OnInit, Output} from '@angular/core';

@Component({
  selector: 'app-tag',
  templateUrl: './tag.component.html',
  styleUrls: ['./tag.component.css']
})
export class TagComponent implements OnInit {
  @Input() blogId: string;
  @Output() onChanged: EventEmitter<any> = new EventEmitter();
  tags: string[] = [];
  tagText: string;
  constructor() { }

  ngOnInit() {
  }
  addTags() {
    if (this.tagText !== '') {
      const tagList = this.tagText.split(',');
      this.tagText = '';
      tagList.forEach(value => {
        this.tags.push(value.trim());
        this.onChanged.emit(this.tags);
      });
    }
  }
  removeTag(tagName) {
    this.tags.map((value, index, array) => {
      if (value === tagName) {
        this.tags.splice(index, 1);
        this.onChanged.emit(this.tags);
      }
    });
  }

}
