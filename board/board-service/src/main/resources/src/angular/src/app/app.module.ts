import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { RoutingModule } from './routing.module';
import { HttpModule } from '@angular/http';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { MarkdownModule } from 'ngx-markdown';
import {BrowserAnimationsModule} from '@angular/platform-browser/animations';
import {MatDialogModule} from '@angular/material/dialog';

import { CommonService } from './main/common.service';

import { AppComponent } from './app.component';
import { ListComponent } from './main/list/list.component';
import { PostComponent } from './main/post/post.component';
import { CommentComponent } from './main/comment/comment.component';
import { CommentListComponent } from './main/comment-list/comment-list.component';
import { WriteComponent } from './main/write/write.component';
import { CategoryComponent } from './main/category/category.component';
import { DialogComponent } from './main/category/dialog/dialog.component';
import { TagComponent } from './main/tag/tag.component';

@NgModule({
  declarations: [
    AppComponent,
    ListComponent,
    PostComponent,
    CommentComponent,
    CommentListComponent,
    WriteComponent,
    CategoryComponent,
    DialogComponent,
    TagComponent
  ],
  entryComponents: [DialogComponent],
  imports: [
    BrowserModule,
    BrowserAnimationsModule,
    MatDialogModule,
    RoutingModule,
    HttpModule,
    FormsModule,
    ReactiveFormsModule,
    MarkdownModule.forRoot()
  ],
  providers: [CommonService],
  bootstrap: [AppComponent]
})
export class AppModule { }
