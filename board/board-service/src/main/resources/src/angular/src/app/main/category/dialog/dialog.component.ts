import { Component, OnInit, Inject } from '@angular/core';
import {MatDialogRef, MAT_DIALOG_DATA} from '@angular/material';
import {Http} from '@angular/http';

@Component({
  selector: 'app-dialog',
  templateUrl: './dialog.component.html',
  styleUrls: ['./dialog.component.css']
})
export class DialogComponent implements OnInit {
  categoryName: string;
  constructor(
    public dialogRef: MatDialogRef<DialogComponent>,
    @Inject(MAT_DIALOG_DATA) public data: any,
    private http: Http
  ) { }

  ngOnInit() {
    console.log(this.data);
  }
  add() {
    if (this.categoryName && this.categoryName !== '') {
      const postCategoryUrl = `http://localhost:8080/board/api/s/categories`;
      const body = {
        blogId: this.data.blogId,
        name: this.categoryName
      }
      this.http.post(postCategoryUrl, body).map(data => data.text()).subscribe(data => {
        console.log(data);
        this.dialogRef.close(true);
      });
    }
  }
  cancel() {
    this.dialogRef.close(false);
  }

}
