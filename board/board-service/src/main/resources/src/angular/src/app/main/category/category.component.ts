import {Component, EventEmitter, Input, OnInit, Output, Inject} from '@angular/core';
import {MatDialog} from '@angular/material';
import {Http} from '@angular/http';
import {DialogComponent} from './dialog/dialog.component';

@Component({
  selector: 'app-category',
  templateUrl: './category.component.html',
  styleUrls: ['./category.component.css']
})
export class CategoryComponent implements OnInit {
  @Input() blogId: string;
  @Input() mode: string;
  @Output() onCategoryClick: EventEmitter<any>;
  categorys: {}[];
  selectedCategory: string;
  constructor(
    private http: Http,
    private matDialog: MatDialog) {
    this.onCategoryClick = new EventEmitter();
  }

  ngOnInit() {
    const getCategoryUrl = `http://localhost:8080/board/api/s/categories?blogId=${this.blogId}`;
    this.http.get(getCategoryUrl).map(data => data.json()).subscribe(data => {
      this.categorys = data;
    });
  }
  clickCategory($event, id) {
    $event.preventDefault();
    this.selectedCategory = id;
    this.onCategoryClick.emit(id);
  }
  clickCreateCategory($event) {
    $event.preventDefault();
    const dialog = this.matDialog.open(DialogComponent, {
      width: '250px',
      data: {blogId : this.blogId}
    });
    dialog.afterClosed().subscribe((data) => {
      if (data) {
        this.ngOnInit();
      }
    });
  }
}
