import {EventEmitter, Injectable} from '@angular/core';
import {Http} from '@angular/http';

@Injectable()
export class CommonService {

  userId: string;
  userName: string;
  blogId: string;
  blogData: EventEmitter<any>;
  constructor(private http: Http) {
    this.userId = 'spectra';
    this.userName = '스펙트라';
    this.blogData = new EventEmitter();
    this.http.get(`http://localhost:8080/board/api/s/blogs?userId=${this.userId}`).subscribe( (data) => {
      const blogInfo = data.json();
      this.blogId = blogInfo.id;
      this.blogData.emit(blogInfo);
    });
  }
}
