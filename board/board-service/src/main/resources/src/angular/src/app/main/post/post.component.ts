import {Component, OnInit, ViewChild} from '@angular/core';
import {ActivatedRoute} from '@angular/router';
import {Http} from '@angular/http';
import 'rxjs/add/operator/map';
import {CommonService} from '../common.service';
import * as moment from 'moment';

@Component({
  selector: 'app-post',
  templateUrl: './post.component.html',
  styleUrls: ['./post.component.css']
})
export class PostComponent implements OnInit {

  userId: string;
  userName: string;
  postId: string;
  post: any;
  commentMessage: string;
  @ViewChild('commentCount') commentCountEl;
  @ViewChild('commentList') commentListEl;
  constructor(
    private route: ActivatedRoute,
    private http: Http,
    private commonService: CommonService) { }

  ngOnInit() {
    this.postId = this.route.snapshot.paramMap.get('postId');
    this.userId = this.commonService.userId;
    this.userName = this.commonService.userName;
    const getPostUrl = `http://localhost:8080/board/api/s/posts/${this.postId}`;
    this.http.get(getPostUrl).map(data => data.json()).subscribe((data) => {
      console.log(data);
      this.post = data;
    });
  }
  getDate(date: string) {
    return moment(date, 'YYYYMMddHHmmss').toDate();
  }
  saveComment() {
    const body = {
      content: this.commentMessage,
      postId: this.postId,
      userId: this.userId,
      userName: this.userName
    };
    this.http.post('http://localhost:8080/board/api/s/comments', body).map(data => data.text()).subscribe( data => {
      this.commentMessage = '';
      this.commentCountEl.loadComment();
      this.commentListEl.loadComment();
    });
  }
  clickCategory($event) {
    console.log($event);
  }
}
