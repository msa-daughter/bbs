import { Component, OnInit } from '@angular/core';
import {FormBuilder, FormGroup} from '@angular/forms';
import {ActivatedRoute, Route, Router} from '@angular/router';
import {Http} from '@angular/http';
import {CommonService} from '../common.service';

@Component({
  selector: 'app-write',
  templateUrl: './write.component.html',
  styleUrls: ['./write.component.css']
})
export class WriteComponent implements OnInit {
  blogId: string;
  writeFormGroup: FormGroup;
  constructor(
    private route: ActivatedRoute,
    private http: Http,
    private commonService: CommonService,
    private router: Router
  ) {
    this.blogId = this.route.snapshot.paramMap.get('blogId');
    const fb = new FormBuilder();
    this.writeFormGroup = fb.group({
      title: [''],
      content: [''],
      category: [''],
      tagNames: ['']
    });
  }

  ngOnInit() {
  }
  writePost() {
    const body = {
      categoryId: this.writeFormGroup.value.category,
      content: this.writeFormGroup.value.content,
      tagNames: this.writeFormGroup.value.tagNames,
      title: this.writeFormGroup.value.title,
      userId: this.commonService.userId
    }
    const postPostUrl = `http://localhost:8080/board/api/s/posts`;
    this.http.post(postPostUrl, body).map(data => data.text()).subscribe(data => {
      this.router.navigate([`/list/${this.blogId}`]);
    });
  }
  clickCategory(id) {
    this.writeFormGroup.value.category = id;
  }
  changedTags(tags) {
    this.writeFormGroup.value.tagNames = tags;
  }
}
