import {Component, Input, OnInit} from '@angular/core';
import {Http} from '@angular/http';
import * as moment from 'moment';

@Component({
  selector: 'app-comment-list',
  templateUrl: './comment-list.component.html',
  styleUrls: ['./comment-list.component.css']
})
export class CommentListComponent implements OnInit {
  @Input() postId: string;
  comments: {}[];
  constructor(
    private http: Http
  ) { }

  ngOnInit() {
    this.loadComment();
  }
  loadComment() {
    const commentUrl = `http://localhost:8080/board/api/s/comments?postId=${this.postId}`;
    this.http.get(commentUrl).map(data => data.json()).subscribe(data => {
      console.log(data);
      this.comments = data;
    });
  }
  getDate(date: string) {
    return moment(date, 'YYYYMMddHHmmss').toDate();
  }

}
