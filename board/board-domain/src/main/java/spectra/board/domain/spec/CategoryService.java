package spectra.board.domain.spec;

import spectra.board.domain.entity.Category;
import spectra.board.domain.spec.sdo.CategoryCdo;
import spectra.share.domain.NameValueList;

import java.util.List;

public interface CategoryService {
     //
     String registerCategory(CategoryCdo categoryCdo);
     Category findCategory(String categoryId);
     List<Category> findCategoryByBlogId(String blogId);
     List<Category> findCategoryByUserId(String userId);
     void modifyCategory(String categoryId, NameValueList nameValues);
     void removeCategory(String categotyId);
}
