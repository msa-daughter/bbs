package spectra.board.domain.store;

import spectra.board.domain.entity.Post;

import java.util.List;
import java.util.NoSuchElementException;

public interface PostStore {
    //
    String create(Post post);
    Post retrieve(String id) throws NoSuchElementException;
    List<Post> retrieveByBlogId(String blogId, int offset, int limit);
    List<Post> retrieveByCategoryId(String categoryId, int offset, int limit);
    List<Post> retrieveByContent(String content, int offset, int limit);
    List<Post> retrieveByTitle(String title, int offset, int limit);
    List<Post> retrieveByTitleAndContent(String title, String content, int offset, int limit);
    void update(Post post);
    void delete(Post post);
    void delete(String id);
}
