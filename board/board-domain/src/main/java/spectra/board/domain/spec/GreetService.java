package spectra.board.domain.spec;

import spectra.board.domain.entity.Greet;

public interface GreetService {
    Greet sayHello(String messageId);
}
