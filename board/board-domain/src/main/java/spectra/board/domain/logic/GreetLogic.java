package spectra.board.domain.logic;

import spectra.board.domain.entity.Greet;
import spectra.board.domain.spec.GreetProvider;
import spectra.board.domain.spec.GreetService;
import spectra.board.domain.store.GreetStore;
import spectra.board.domain.store.GreetStoreLycler;

public class GreetLogic implements GreetService, GreetProvider{
    //
    private GreetStore greetStore;

    public GreetLogic(GreetStoreLycler greetStoreLyclear ){
        this.greetStore = greetStoreLyclear.requestGreetStore();
    }

    @Override
    public Greet sayHello(String messageId) {
        //
        Greet greet = greetStore.retrieve(messageId);
        return greet;
    }
}
