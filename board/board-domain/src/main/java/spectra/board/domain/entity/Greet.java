package spectra.board.domain.entity;

import spectra.share.domain.Aggregate;
import spectra.share.domain.Entity;

public class Greet  extends Entity implements Aggregate {

    private String message;

    public Greet(){
        //
    }

    public Greet(String message){
        this.message = message;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }
}
