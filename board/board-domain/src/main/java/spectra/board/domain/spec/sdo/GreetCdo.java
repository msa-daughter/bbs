package spectra.board.domain.spec.sdo;

import spectra.share.util.json.JsonUtil;

import java.io.Serializable;

public class GreetCdo implements Serializable {

    private String message;

    public GreetCdo(){
        //
    }

    public GreetCdo(String message){
        this.message = message;
    }

    @Override
    public String toString() {
        final StringBuilder sb = new StringBuilder("GreetCdo{");
        sb.append("message = ").append(this.message).append('\'');
        sb.append('}');
        return sb.toString();
    }

    public static GreetCdo getSample(){
        String message = "Hello World";
        GreetCdo sample = new GreetCdo(message);
        return sample;
    }

    public String toJson() {
        //
        return JsonUtil.toJson(this);
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public static void main(String[] args) {
        //
        System.out.println(getSample());
    }
}
