package spectra.board.domain.spec;

import spectra.board.domain.entity.Comment;
import spectra.board.domain.spec.sdo.CommentCdo;
import spectra.share.domain.NameValueList;

import java.util.List;

public interface CommentService {
    //
    String registerComment(CommentCdo commentCdo);
    Comment findComment(String commentId);
    List<Comment> findCommentByPostId(String postId);
    void modifyComment(String commentId, NameValueList nameValues);
    void removeComment(String commentId);
}
