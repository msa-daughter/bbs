package spectra.board.domain.spec.sdo;

import spectra.share.util.json.JsonUtil;

public class CommentCdo {

	private String content;

	private String postId;
	private String userId;
	private String userName;

    public CommentCdo() {
        //
    }

	@Override
	public String toString() {
		final StringBuilder sb = new StringBuilder("CommentCdo{");
		sb.append("content='").append(content).append('\'');
		sb.append(", postId='").append(postId).append('\'');
		sb.append(", userId='").append(userId).append('\'');
		sb.append(", userName='").append(userName).append('\'');
		sb.append('}');
		return sb.toString();
	}

	public static CommentCdo getSample() {
		CommentCdo commentCdo = new CommentCdo();
		return commentCdo;
	}

	public String toJson() {
		//
		return JsonUtil.toJson(this);
	}

	public static BlogCdo fromJson(String json) {
		//
		return JsonUtil.fromJson(json, BlogCdo.class);
	}

	public String getPostId() {
		return postId;
	}

	public void setPostId(String postId) {
		this.postId = postId;
	}

	public String getUserId() {
		return userId;
	}

	public void setUserId(String userId) {
		this.userId = userId;
	}

	public String getUserName() {
		return userName;
	}

	public void setUserName(String userName) {
		this.userName = userName;
	}

	public String getContent() {
		return content;
	}

	public void setContent(String content) {
		this.content = content;
	}
}
