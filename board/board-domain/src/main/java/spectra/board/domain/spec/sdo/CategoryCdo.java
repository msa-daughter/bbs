package spectra.board.domain.spec.sdo;

import spectra.share.util.json.JsonUtil;

public class CategoryCdo {
	//
	private String name;
	private String blogId;

    public CategoryCdo() {
        //
    }

    public CategoryCdo(String name, String blogId) {
    	super();
    	this.name = name;
    	this.blogId = blogId;
	}

	@Override
	public String toString() {
		final StringBuilder sb = new StringBuilder("CategoryCdo{");
		sb.append("name='").append(name).append('\'');
		sb.append(", blogId='").append(blogId).append('\'');
		sb.append('}');
		return sb.toString();
	}

	public static CategoryCdo getSample() {
		CategoryCdo categoryCdo = new CategoryCdo();
		return categoryCdo;
	}

	public String toJson() {
		//
		return JsonUtil.toJson(this);
	}

	public static BlogCdo fromJson(String json) {
		//
		return JsonUtil.fromJson(json, BlogCdo.class);
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getBlogId() {
		return blogId;
	}

	public void setBlogId(String blogId) {
		this.blogId = blogId;
	}
}
