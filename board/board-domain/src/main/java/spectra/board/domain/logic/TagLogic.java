package spectra.board.domain.logic;

import spectra.board.domain.entity.Blog;
import spectra.board.domain.entity.Tag;
import spectra.board.domain.spec.TagService;
import spectra.board.domain.spec.sdo.TagCdo;
import spectra.board.domain.store.BlogStore;
import spectra.board.domain.store.BoardStoreLycler;
import spectra.board.domain.store.TagStore;

import java.util.List;
import java.util.NoSuchElementException;

public class TagLogic implements TagService {
    //
    private BlogStore blogStore;
    private TagStore tagStore;

    public TagLogic(BoardStoreLycler boardStoreLycler) {
        blogStore = boardStoreLycler.requestBlogStore();
        tagStore = boardStoreLycler.requestTagStore();
    }

    @Override
    public String registerTag(TagCdo tagCdo)
    {
        if (tagCdo.getBlogId() == null || "".equals(tagCdo.getBlogId()) ) {
            throw new NoSuchElementException(String.format("blog id:%s", tagCdo.getBlogId()));
        }
        if (tagCdo.getTagName() == null || "".equals(tagCdo.getTagName()) ) {
            throw new NoSuchElementException(String.format("tag name:%s", tagCdo.getTagName()));
        }
        Blog blog = blogStore.retrieve(tagCdo.getBlogId());
        if (blog == null) {
            throw new NoSuchElementException(String.format("blog id:%s", tagCdo.getBlogId()));
        }
        return tagStore.create(new Tag(tagCdo.getTagName(), tagCdo.getBlogId()));
    }

    @Override
    public List<Tag> findTagByBlogId(String blogId, int offset, int limit) {
        return tagStore.retrieveByBlogId(blogId, offset, limit);
    }
}
