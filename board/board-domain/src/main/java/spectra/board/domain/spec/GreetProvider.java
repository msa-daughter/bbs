package spectra.board.domain.spec;

import spectra.board.domain.entity.Greet;

public interface GreetProvider {
    Greet sayHello(String messageId);
}
