package spectra.board.domain.entity;

import spectra.share.domain.Entity;
import spectra.share.domain.NameValue;
import spectra.share.domain.NameValueList;
import spectra.share.util.date.DateUtil;
import spectra.share.util.json.JsonUtil;

public class Blog extends Entity {

    private String blogName;
    private String description;
    private String userId;
    private String userName;
    private String createdDate;

    public Blog() {
        //
    }

    public Blog(String id) {
        //
        super(id);
    }

    public Blog(String blogName, String userId, String userName) {
        super();
        this.blogName = blogName;
        this.description = "";
        this.userId = userId;
        this.userName = userName;
        this.createdDate = DateUtil.now();
    }

    @Override
    public String toString() {
        final StringBuilder sb = new StringBuilder("Blog{");
        sb.append("blogName='").append(blogName).append('\'');
        sb.append(", description='").append(description).append('\'');
        sb.append(", userId='").append(userId).append('\'');
        sb.append(", userName='").append(userName).append('\'');
        sb.append(", createdDate=").append(createdDate);
        sb.append('}');
        return sb.toString();
    }

    public static Blog getSample() {
        Blog blog = new Blog();
        return blog;
    }

    public void setValues(NameValueList nameValues) {
        //
        for(NameValue nameValue : nameValues.getList()) {
            String value = nameValue.getValue();
            switch(nameValue.getName()) {
                case "blogName":        this.blogName = value; break;
                case "description": this.description = value; break;
            }
        }
    }

    public String toJson() {
        //
        return JsonUtil.toJson(this);
    }

    public String getBlogName() {
        return blogName;
    }

    public void setBlogName(String blogName) {
        this.blogName = blogName;
    }

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getCreatedDate() {
        return createdDate;
    }

    public void setCreatedDate(String createdDate) {
        this.createdDate = createdDate;
    }
}
