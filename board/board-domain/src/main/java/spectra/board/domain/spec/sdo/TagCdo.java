package spectra.board.domain.spec.sdo;

import spectra.share.util.json.JsonUtil;

public class TagCdo {
    //
    private String tagName;
    private String blogId;

    public TagCdo() {
        //
    }

    public TagCdo(String blogId, String tagName) {
        this.blogId = blogId;
        this.tagName = tagName;
    }

    @Override
    public String toString() {
        final StringBuilder sb = new StringBuilder("TagCdo{");
        sb.append("tagName='").append(tagName).append('\'');
        sb.append(", blogId='").append(blogId).append('\'');
        sb.append('}');
        return sb.toString();
    }

    public static TagCdo getSample() {
        TagCdo postCdo = new TagCdo();
        return postCdo;
    }

    public String toJson() {
        //
        return JsonUtil.toJson(this);
    }

    public static BlogCdo fromJson(String json) {
        //
        return JsonUtil.fromJson(json, BlogCdo.class);
    }

    public String getTagName() {
        return tagName;
    }

    public void setTagName(String tagName) {
        this.tagName = tagName;
    }

    public String getBlogId() {
        return blogId;
    }

    public void setBlogId(String blogId) {
        this.blogId = blogId;
    }
}
