package spectra.board.domain.store;

import spectra.board.domain.entity.Category;

import java.util.List;
import java.util.NoSuchElementException;

public interface CategoryStore {
    //
    String create(Category category);
    Category retrieve(String id) throws NoSuchElementException;
    List<Category> retrieveByBlogId(String blogId);
    void update(Category category);
    void delete(Category category);
    void delete(String id);
}
