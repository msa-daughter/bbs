package spectra.board.domain.store;

import spectra.board.domain.entity.Comment;

import java.util.List;
import java.util.NoSuchElementException;

public interface CommentStore {
    //
    String create(Comment comment);
    Comment retrieve(String id) throws NoSuchElementException;
    List<Comment> retrieveByPostId(String postId);
    void update(Comment comment);
    void delete(Comment comment);
    void delete(String id);
}
