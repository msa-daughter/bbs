package spectra.board.domain.spec;

import spectra.board.domain.entity.Tag;
import spectra.board.domain.spec.sdo.TagCdo;

import java.util.List;

public interface TagService {
   //
   String registerTag(TagCdo tagCdo);
   List<Tag> findTagByBlogId(String blogId, int offset, int limit);
}
