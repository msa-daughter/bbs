package spectra.board.domain.spec;

import spectra.board.domain.entity.Blog;
import spectra.board.domain.spec.sdo.BlogCdo;
import spectra.share.domain.NameValueList;

public interface BlogService {
    //
    String createBlog(BlogCdo blogCdo);
    Blog findBlog(String blogId);
    Blog findBlogByUserId(String userId);
    void modifyBlog(String blogId, NameValueList nameValues);
    void removeBlog(String blogId);
}
