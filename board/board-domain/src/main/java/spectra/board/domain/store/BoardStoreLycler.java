package spectra.board.domain.store;

public interface BoardStoreLycler {
    //
    BlogStore requestBlogStore();
    CategoryStore requestCategoryStore();
    CommentStore requestCommentStore();
    PostStore requestPostStore();
    //PostTagStore requestPostTagStore();
    TagStore requestTagStore();
}
