package spectra.board.domain.store;

import spectra.board.domain.entity.Tag;

import java.util.List;
import java.util.NoSuchElementException;

public interface TagStore {
    //
    String create(Tag tag);
    Tag retrieve(String id) throws NoSuchElementException;
    Tag retrieveByTagName(String tagName);
    List<Tag> retrieveByBlogId(String blogId, int offset, int limit);
    void update(Tag tag);
}
