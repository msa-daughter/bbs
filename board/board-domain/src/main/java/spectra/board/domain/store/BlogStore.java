package spectra.board.domain.store;

import spectra.board.domain.entity.Blog;

import java.util.NoSuchElementException;

public interface BlogStore {
    //
    String create(Blog blog);
    Blog retrieve(String id) throws NoSuchElementException;
    Blog retrieveByUserId(String userId);
    void update(Blog blog);
    void delete(String blogId);
    void delete(Blog blog);
}
