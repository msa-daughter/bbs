package spectra.board.domain.logic;

import spectra.board.domain.entity.Comment;
import spectra.board.domain.spec.CommentService;
import spectra.board.domain.spec.sdo.CommentCdo;
import spectra.board.domain.store.BoardStoreLycler;
import spectra.board.domain.store.CommentStore;
import spectra.share.domain.NameValueList;

import java.util.List;

public class CommentLogic implements CommentService {
    //
    private CommentStore commentStore;

    public CommentLogic(BoardStoreLycler boardStoreLycler) {
        commentStore = boardStoreLycler.requestCommentStore();
    }
    @Override
    public String registerComment(CommentCdo commentCdo) {
        String content = commentCdo.getContent();
        String postId = commentCdo.getPostId();
        String userId = commentCdo.getUserId();
        String userName =commentCdo.getUserName();

        Comment comment = new Comment(postId, userId, userName, content);

        commentStore.create(comment);

        return comment.getId();
    }

    @Override
    public Comment findComment(String commentId) {
        return commentStore.retrieve(commentId);
    }

    @Override
    public List<Comment> findCommentByPostId(String postId) {
        return commentStore.retrieveByPostId(postId);
    }

    @Override
    public void modifyComment(String commentId, NameValueList nameValues) {
        Comment comment = findComment(commentId);
        comment.setValues(nameValues);

        commentStore.update(comment);
    }

    @Override
    public void removeComment(String commentId) {
        commentStore.delete(commentId);
    }
}
