package spectra.board.domain.entity;

import spectra.share.domain.Entity;
import spectra.share.domain.NameValue;
import spectra.share.domain.NameValueList;
import spectra.share.util.date.DateUtil;
import spectra.share.util.json.JsonUtil;

public class Category extends Entity {
	//
	private String categoryName;
	private int useCount;
	private String createdDate;

	private String blogId;

    public Category() {
        //
    }

	public Category(String id) {
		super(id);
	}

	public Category(String categoryName, String blogId) {
    	super();
		this.categoryName = categoryName;
		this.useCount = 0;
		this.blogId = blogId;
		this.createdDate = DateUtil.now();
	}

	@Override
	public String toString() {
		final StringBuilder sb = new StringBuilder("Category{");
		sb.append("categoryName='").append(categoryName).append('\'');
		sb.append(", useCount=").append(useCount);
		sb.append(", createdDate='").append(createdDate).append('\'');
		sb.append(", blogId='").append(blogId).append('\'');
		sb.append('}');
		return sb.toString();
	}

	public static Category getSample() {
		Category category = new Category();
		return category;
	}

	public void setValues(NameValueList nameValues) {
		//
		for(NameValue nameValue : nameValues.getList()) {
			String value = nameValue.getValue();
			switch(nameValue.getName()) {
				case "categoryName": this.categoryName = value; break;
			}
		}
	}

	public String toJson() {
		//
		return JsonUtil.toJson(this);
	}

	public String getCategoryName() {
		return categoryName;
	}

	public void setCategoryName(String categoryName) {
		this.categoryName = categoryName;
	}

	public int getUseCount() {
		return useCount;
	}

	public void setUseCount(int useCount) {
		this.useCount = useCount;
	}

	public String getCreatedDate() {
		return createdDate;
	}

	public void setCreatedDate(String createdDate) {
		this.createdDate = createdDate;
	}

	public String getBlogId() {
		return blogId;
	}

	public void setBlogId(String blogId) {
		this.blogId = blogId;
	}
}
