package spectra.board.domain.spec.sdo;

import spectra.share.util.json.JsonUtil;

import java.util.Arrays;
import java.util.List;

public class PostCdo {
    //
    private String title;
    private String content;

    private String userId;
    private String categoryId;

    private List<String> tagNames;

    public PostCdo() {
        //
    }

    public PostCdo(String userId, String categoryId, String title, String content, List<String> tagNames) {
        this.userId = userId;
        this.categoryId = categoryId;
        this.title = title;
        this.content = content;
        this.tagNames = tagNames;
    }

    @Override
    public String toString() {
        final StringBuilder sb = new StringBuilder("PostCdo{");
        sb.append("title='").append(title).append('\'');
        sb.append(", content='").append(content).append('\'');
        sb.append(", userId='").append(userId).append('\'');
        sb.append(", categoryId='").append(categoryId).append('\'');
        sb.append('}');
        return sb.toString();
    }

    public static PostCdo getSample() {
        PostCdo postCdo = new PostCdo("spectra", "cate1", "제목", "내용", Arrays.asList("태그1","태그2"));
        return postCdo;
    }

    public String toJson() {
        //
        return JsonUtil.toJson(this);
    }

    public static BlogCdo fromJson(String json) {
        //
        return JsonUtil.fromJson(json, BlogCdo.class);
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }

    public String getCategoryId() {
        return categoryId;
    }

    public void setCategoryId(String categoryId) {
        this.categoryId = categoryId;
    }

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public List<String> getTagNames() {
        return tagNames;
    }
}
