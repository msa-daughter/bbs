package spectra.board.domain.store;

import spectra.board.domain.entity.Greet;

import java.util.NoSuchElementException;

public interface GreetStore {
    //
    String create(Greet greet);
    Greet retrieve(String id) throws NoSuchElementException;
    void update(Greet greet);
    void delete(Greet greet);
    void delete(String id);
}
