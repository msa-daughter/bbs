package spectra.board.domain.entity;

import spectra.share.domain.Entity;
import spectra.share.domain.NameValue;
import spectra.share.domain.NameValueList;
import spectra.share.util.date.DateUtil;
import spectra.share.util.json.JsonUtil;

public class Comment extends Entity {

	private String content;
	private String createdDate;

	private String postId;
	private String userId;
	private String userName;

    public Comment() {
        //
    }

	public Comment(String id) {
		super(id);
	}

	public Comment(String postId, String userId, String userName, String content) {
		super();
		this.content = content;
		this.createdDate = DateUtil.now();
		this.postId = postId;
		this.userId = userId;
		this.userName = userName;
	}

	@Override
	public String toString() {
		final StringBuilder sb = new StringBuilder("Comment{");
		sb.append("content='").append(content).append('\'');
		sb.append(", createdDate=").append(createdDate);
		sb.append(", postId='").append(postId).append('\'');
		sb.append(", userId='").append(userId).append('\'');
		sb.append(", userName='").append(userName).append('\'');
		sb.append('}');
		return sb.toString();
	}

	public static Comment getSample() {
		Comment comment = new Comment();
		return comment;
	}

	public void setValues(NameValueList nameValues) {
		//
		for(NameValue nameValue : nameValues.getList()) {
			String value = nameValue.getValue();
			switch(nameValue.getName()) {
				case "content":	this.content = value; break;
			}
		}
	}

	public String toJson() {
		//
		return JsonUtil.toJson(this);
	}

	public String getContent() {
		return content;
	}

	public void setContent(String content) {
		this.content = content;
	}

	public String getCreatedDate() {
		return createdDate;
	}

	public void setCreatedDate(String createdDate) {
		this.createdDate = createdDate;
	}

	public String getPostId() {
		return postId;
	}

	public void setPostId(String postId) {
		this.postId = postId;
	}

	public String getUserId() {
		return userId;
	}

	public void setUserId(String userId) {
		this.userId = userId;
	}

	public String getUserName() {
		return userName;
	}

	public void setUserName(String userName) {
		this.userName = userName;
	}
}
