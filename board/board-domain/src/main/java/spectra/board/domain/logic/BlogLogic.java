package spectra.board.domain.logic;

import spectra.board.domain.entity.Blog;
import spectra.board.domain.spec.BlogService;
import spectra.board.domain.spec.sdo.BlogCdo;
import spectra.board.domain.store.BlogStore;
import spectra.board.domain.store.BoardStoreLycler;
import spectra.share.domain.NameValueList;

import java.util.NoSuchElementException;

public class BlogLogic implements BlogService {
    //
    private BlogStore blogStore;

    public BlogLogic(BoardStoreLycler boardStoreLycler) {
        this.blogStore = boardStoreLycler.requestBlogStore();
    }

    @Override
    public String createBlog(BlogCdo blogCdo) {
        String blogName = blogCdo.getBlogName();
        String userId = blogCdo.getUserId();
        String userName = blogCdo.getUserName();
        String description = blogCdo.getDescription();

        Blog blog = new Blog(blogName, userId, userName);
        blog.setDescription(description);

        blogStore.create(blog);
        return blog.getId();
    }

    @Override
    public Blog findBlog(String blogId) {
        Blog blog = blogStore.retrieve(blogId);
        if (blog == null) {
            throw new NoSuchElementException(String.format("blogId:%s", blogId));
        }

        return blog;
    }

    @Override
    public Blog findBlogByUserId(String userId) {
        Blog blog = blogStore.retrieveByUserId(userId);
        if (blog == null) {
            throw new NoSuchElementException(String.format("userId:%s", userId));
        }

        return blog;
    }

    @Override
    public void modifyBlog(String blogId, NameValueList nameValues) {
        //
        Blog blog = findBlog(blogId);
        blog.setValues(nameValues);

        blogStore.update(blog);
    }

    @Override
    public void removeBlog(String blogId) {
        blogStore.delete(blogId);
    }
}
