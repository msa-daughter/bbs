package spectra.board.domain.spec.sdo;

import spectra.share.util.json.JsonUtil;

public class BlogCdo {
    //
    private String blogName;
    private String description;
    private String userId;
    private String userName;

    public BlogCdo() {
        //
    }

    public BlogCdo(String blogName, String description, String userId, String userName){
        this.blogName = blogName;
        this.description = description;
        this.userId = userId;
        this.userName = userName;
    }

    @Override
    public String toString() {
        final StringBuilder sb = new StringBuilder("BlogCdo{");
        sb.append("blogName='").append(blogName).append('\'');
        sb.append(", description='").append(description).append('\'');
        sb.append(", userId='").append(userId).append('\'');
        sb.append(", userName='").append(userName).append('\'');
        sb.append('}');
        return sb.toString();
    }

    public static BlogCdo getSample() {
        BlogCdo blogCdo = new BlogCdo();
        return blogCdo;
    }

    public String toJson() {
        //
        return JsonUtil.toJson(this);
    }

    public static BlogCdo fromJson(String json) {
        //
        return JsonUtil.fromJson(json, BlogCdo.class);
    }

    public String getBlogName() {
        return blogName;
    }

    public void setBlogName(String blogName) {
        this.blogName = blogName;
    }

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }
    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }
}
