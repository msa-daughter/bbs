package spectra.board.domain.entity;

import spectra.share.domain.Entity;
import spectra.share.domain.NameValue;
import spectra.share.domain.NameValueList;
import spectra.share.util.date.DateUtil;
import spectra.share.util.json.JsonUtil;
import spectra.share.util.string.StringUtil;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class Post extends Entity {
	//
	private String title;
	private String content;
	private String createdDate;
	private String updatedDate;

	private String blogId;
	private String categoryId;
	private Category category;

	private List<String> tagNames;

	public Post() {
		//
	}

	public Post(String id) {
		super(id);
	}

	public Post(String title, String content, String blogId, String categoryId) {
	    super();
		this.title = title;
		this.content = content;
		this.createdDate = DateUtil.now();
		this.updatedDate = DateUtil.now();

		this.blogId = blogId;
		this.categoryId = categoryId;
		this.tagNames = new ArrayList<>();
	}

	@Override
	public String toString() {
		final StringBuilder sb = new StringBuilder("Post{");
		sb.append("title='").append(title).append('\'');
		sb.append(", content='").append(content).append('\'');
		sb.append(", createdDate='").append(createdDate).append('\'');
		sb.append(", updatedDate='").append(updatedDate).append('\'');
		sb.append(", blogId='").append(blogId).append('\'');
		sb.append(", categoryId='").append(categoryId).append('\'');
		sb.append(", category=").append(category);
		sb.append(", tagNames=").append(tagNames);
		sb.append('}');
		return sb.toString();
	}

	public static Post getSample() {
		Post post = new Post();
		return post;
	}

    public void setValues(NameValueList nameValues) {
		//
		for(NameValue nameValue : nameValues.getList()) {
			String value = nameValue.getValue();
			switch(nameValue.getName()) {
				case "title":		this.title = value; break;
				case "content":		this.content = value; break;
				case "categoryId":	this.categoryId = value; break;
				case "blogId":		this.blogId = value; break;
				case "tagNames":	this.tagNames = Arrays.asList(value.split(StringUtil.SPACE)); break;
			}
		}
	}

	public String toJson() {
		//
		return JsonUtil.toJson(this);
	}

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public String getContent() {
		return content;
	}

	public void setContent(String content) {
		this.content = content;
	}

	public String getCategoryId() {
		return categoryId;
	}

	public void setCategoryId(String categoryId) {
		this.categoryId = categoryId;
	}

	public String getCreatedDate() {
		return createdDate;
	}

	public void setCreatedDate(String createdDate) {
		this.createdDate = createdDate;
	}

	public String getUpdatedDate() {
		return updatedDate;
	}

	public void setUpdatedDate(String updatedDate) {
		this.updatedDate = updatedDate;
	}

	public String getBlogId() {
		return blogId;
	}

	public void setBlogId(String blogId) {
		this.blogId = blogId;
	}

	public Category getCategory() {
		return category;
	}

	public void setCategory(Category category) {
		this.category = category;
	}

	public List<String> getTagNames() {
		return tagNames;
	}

	public void setTagNames(List<String> tagNames) {
		this.tagNames = tagNames;
	}
}
