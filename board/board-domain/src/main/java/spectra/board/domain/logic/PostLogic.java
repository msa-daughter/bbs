package spectra.board.domain.logic;

import spectra.board.domain.entity.Blog;
import spectra.board.domain.entity.Category;
import spectra.board.domain.entity.Post;
import spectra.board.domain.entity.Tag;
import spectra.board.domain.spec.PostService;
import spectra.board.domain.spec.sdo.PostCdo;
import spectra.board.domain.store.*;
import spectra.share.domain.NameValueList;

import java.util.ArrayList;
import java.util.List;
import java.util.NoSuchElementException;
import java.util.stream.Collectors;

public class PostLogic implements PostService {
    //
    private BlogStore blogStore;
    private CategoryStore categoryStore;
    private PostStore postStore;
    private TagStore tagStore;

    public PostLogic(BoardStoreLycler boardStoreLycler) {
        blogStore = boardStoreLycler.requestBlogStore();
        categoryStore = boardStoreLycler.requestCategoryStore();
        postStore = boardStoreLycler.requestPostStore();
        tagStore = boardStoreLycler.requestTagStore();
    }

    @Override
    public String registerPost(PostCdo postCdo) {
        String title = postCdo.getTitle();
        String content = postCdo.getContent();
        String categoryId = postCdo.getCategoryId();

        Blog blog = blogStore.retrieveByUserId(postCdo.getUserId());
        if (blog == null) {
            throw new NoSuchElementException(String.format("user id:%s", postCdo.getUserId()));
        }

        Category category = categoryStore.retrieve(postCdo.getCategoryId());
        if (blog == null) {
            throw new NoSuchElementException(String.format("category id:%s", postCdo.getCategoryId()));
        }
        category.setUseCount(category.getUseCount() + 1);
        categoryStore.update(category);

        Post post = new Post(title, content, blog.getId(), categoryId);
        post.setTagNames(postCdo.getTagNames());
        postStore.create(post);

        //Tag list 를 먼저 만든다.
        List<String> tagNames = postCdo.getTagNames();
        for (String tagName: tagNames) {
            Tag tag = tagStore.retrieveByTagName(tagName);
            if (tag != null) {
                tag.setUseCount(tag.getUseCount() + 1);
                tag.addPostIds(post.getId());
                tagStore.update(tag);
            }
            else {
                tag = new Tag(tagName, blog.getId());
                tag.addPostIds(post.getId());
                tagStore.create(tag);
            }
        }

        return post.getId();
    }

    private Post setCategory(Post post) {
        post.setCategory(categoryStore.retrieve(post.getCategoryId()));
        return post;
    }

    private List<Post> setCategorys(List<Post> posts) {
        return posts.stream()
                .map(post->this.setCategory(post))
                .collect(Collectors.toList());
    }

    @Override
    public Post findPost(String postId) {
        Post post = postStore.retrieve(postId);
        post = setCategory(post);

        return post;
    }

    @Override
    public List<Post> findPostByBlogId(String blogId, int offset, int limit) {
        return setCategorys(postStore.retrieveByBlogId(blogId, offset, limit));
    }

    @Override
    public List<Post> findPostByUserId(String userId, int offset, int limit) {
        Blog blog = blogStore.retrieveByUserId(userId);
        return setCategorys(postStore.retrieveByBlogId(blog.getId(), offset, limit));
    }

    @Override
    public List<Post> findPostByTagName(String tagName, int offset, int limit) {
        Tag tag = tagStore.retrieveByTagName(tagName);
        List<String> postIds = tag.getPostIds();
        int effectiveLimit = (postIds.size() - offset > limit) ? limit : postIds.size() - offset;

        List<Post> posts = new ArrayList<>(limit);
        for (int i = offset; i < effectiveLimit; i++) {
            Post post = postStore.retrieve(postIds.get(i));
            posts.add(post);
        }

        return setCategorys(posts);
    }

    @Override
    public List<Post> findPostByCategoryId(String categoryId, int offset, int limit) {
        return setCategorys(postStore.retrieveByCategoryId(categoryId, offset, limit));
    }

    @Override
    public List<Post> findPostByContent(String content, int offset, int limit) {
        return setCategorys(postStore.retrieveByContent(content, offset, limit));
    }

    @Override
    public List<Post> findPostByTitle(String title, int offset, int limit) {
        return setCategorys(postStore.retrieveByTitle(title, offset, limit));
    }

    @Override
    public List<Post> findPostByTitleAndContent(String title, String content, int offset, int limit) {
        return setCategorys(postStore.retrieveByTitleAndContent(title, content, offset, limit));
    }

    @Override
    public void modifyPost(String postId, NameValueList nameValues) {
        Post post = findPost(postId);
        post.setValues(nameValues);

        postStore.update(post);
    }

    @Override
    public void removePost(String postId) {
        postStore.delete(new Post(postId));
    }
}
