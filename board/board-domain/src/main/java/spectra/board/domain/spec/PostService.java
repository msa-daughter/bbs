package spectra.board.domain.spec;

import spectra.board.domain.entity.Post;
import spectra.board.domain.spec.sdo.PostCdo;
import spectra.share.domain.NameValueList;

import java.util.List;

public interface PostService {
    String registerPost(PostCdo postCdo);
    Post findPost(String postId);

    List<Post> findPostByBlogId(String blogId, int offset, int limit);
    List<Post> findPostByUserId(String userId, int offset, int limit);
    List<Post> findPostByCategoryId(String categoryId, int offset, int limit);
    List<Post> findPostByTagName(String tagName, int offset, int limit);
    List<Post> findPostByContent(String content, int offset, int limit);
    List<Post> findPostByTitle(String title, int offset, int limit);
    List<Post> findPostByTitleAndContent(String title, String content, int offset, int limit);
    void modifyPost(String postId, NameValueList nameValues);
    void removePost(String postId);
}
