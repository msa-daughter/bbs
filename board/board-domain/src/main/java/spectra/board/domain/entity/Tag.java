package spectra.board.domain.entity;

import spectra.share.domain.Entity;
import spectra.share.domain.NameValue;
import spectra.share.domain.NameValueList;
import spectra.share.util.date.DateUtil;
import spectra.share.util.json.JsonUtil;

import java.util.ArrayList;
import java.util.List;

public class Tag extends Entity {
	//
	private String tagName;
	private int useCount;
	private String createdDate;

	private String blogId;
	private List<String> postIds;

	public Tag() {
	    //
	}

	public Tag(String id) {
		super(id);
	}

	public Tag(String tagName, String blogId) {
		super();
		this.tagName = tagName;
		this.useCount = 1;
		this.createdDate = DateUtil.now();

		this.blogId = blogId;
		this.postIds = new ArrayList<>();
	}

	@Override
	public String toString() {
		final StringBuilder sb = new StringBuilder("Tag{");
		sb.append("tagName='").append(tagName).append('\'');
		sb.append(", useCount=").append(useCount);
		sb.append(", createdDate=").append(createdDate);
		sb.append(", blogId='").append(blogId).append('\'');
		sb.append('}');
		return sb.toString();
	}

	public static Tag getSample() {
		Tag tag = new Tag();
		return tag;
	}

	public void setValues(NameValueList nameValues) {
		//
		for(NameValue nameValue : nameValues.getList()) {
			String value = nameValue.getValue();
			switch(nameValue.getName()) {
				case "tagName":        this.tagName = value; break;
			}
		}
	}

	public String toJson() {
		//
		return JsonUtil.toJson(this);
	}

	public String getTagName() {
		return tagName;
	}

	public void setTagName(String tagName) {
		this.tagName = tagName;
	}

	public int getUseCount() {
		return useCount;
	}

	public void setUseCount(int useCount) {
		this.useCount = useCount;
	}

	public String getCreatedDate() {
		return createdDate;
	}

	public void setCreatedDate(String createdDate) {
		this.createdDate = createdDate;
	}

	public String getBlogId() {
		return blogId;
	}

	public void setBlogId(String blogId) {
		this.blogId = blogId;
	}

	public List<String> getPostIds() {
		return postIds;
	}

	public void setPostIds(List<String> postIds) {
		this.postIds = postIds;
	}

	public void addPostIds(String postId) {
		this.postIds.add(postId);
	}
}
