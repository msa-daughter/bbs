package spectra.board.domain.logic;

import spectra.board.domain.entity.Blog;
import spectra.board.domain.entity.Category;
import spectra.board.domain.spec.CategoryService;
import spectra.board.domain.spec.sdo.CategoryCdo;
import spectra.board.domain.store.BlogStore;
import spectra.board.domain.store.BoardStoreLycler;
import spectra.board.domain.store.CategoryStore;
import spectra.share.domain.NameValueList;

import java.util.List;
import java.util.NoSuchElementException;

public class CategoryLogic implements CategoryService {
    //
    private BlogStore blogStore;
    private CategoryStore categoryStore;

    public CategoryLogic(BoardStoreLycler boardStoreLycler) {
        blogStore = boardStoreLycler.requestBlogStore();
        categoryStore = boardStoreLycler.requestCategoryStore();
    }

    @Override
    public String registerCategory(CategoryCdo categoryCdo) {
        if(categoryCdo.getName() == null || "".equals(categoryCdo.getName()) ) {
            throw new NoSuchElementException(String.format("category name: %s", categoryCdo.getName()));
        }
        if(categoryCdo.getBlogId() == null || "".equals(categoryCdo.getBlogId()) ) {
            throw new NoSuchElementException(String.format("blog id: %s", categoryCdo.getBlogId()));
        }

        Blog blog = blogStore.retrieve(categoryCdo.getBlogId());
        if (blog == null) {
            throw new NoSuchElementException(String.format("blog id:%s", categoryCdo.getBlogId()));
        }

        Category category = new Category(categoryCdo.getName(), categoryCdo.getBlogId());
        categoryStore.create(category);
        return category.getId();
    }

    @Override
    public Category findCategory(String categoryId) {
        return categoryStore.retrieve(categoryId);
    }

    @Override
    public List<Category> findCategoryByBlogId(String blogId) {
        return categoryStore.retrieveByBlogId(blogId);
    }

    @Override
    public List<Category> findCategoryByUserId(String userId) {
        Blog blog = blogStore.retrieveByUserId(userId);
        return categoryStore.retrieveByBlogId(blog.getId());
    }

    @Override
    public void modifyCategory(String categoryId, NameValueList nameValues) {
        Category category = categoryStore.retrieve(categoryId);
        category.setValues(nameValues);

        categoryStore.update(category);
    }

    @Override
    public void removeCategory(String categoryId) {
        categoryStore.delete(new Category(categoryId));
    }
}
