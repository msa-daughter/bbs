package spectra.board;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import spectra.board.domain.spec.BlogService;
import spectra.board.domain.spec.CategoryService;
import spectra.board.domain.spec.PostService;
import spectra.board.domain.spec.sdo.BlogCdo;
import spectra.board.domain.spec.sdo.CategoryCdo;
import spectra.board.domain.spec.sdo.PostCdo;
import springfox.documentation.swagger2.annotations.EnableSwagger2;

import javax.annotation.PostConstruct;
import java.util.Arrays;

@SpringBootApplication
@EnableSwagger2
public class BoardApplication {
    private static final Logger logger = LoggerFactory.getLogger(BoardApplication.class);

	public static void main(String[] args) {
		SpringApplication.run(BoardApplication.class, args);
	}
/*
    @Bean
	public CommandLineRunner demo (GreetRepository greetRepository){
	    return (args)->{
            logger.info("=========================");
            greetRepository.save(new GreetJpo("1", "Hello"));
            greetRepository.save(new GreetJpo("2", "World"));
            greetRepository.save(new GreetJpo("3", "Hello World"));
            logger.info("=========================");
        };
    }
*/
    @Autowired
    @Qualifier("blogLogic")
    private BlogService blogService;

	@Autowired
    @Qualifier("postLogic")
	private PostService postService;

	@Autowired
    @Qualifier("categoryLogic")
	private CategoryService categoryService;

    // initial data setup
    @PostConstruct
    public void initialize() {
        String blogId1 = blogService.createBlog(new BlogCdo("블로그이름1","블로그설명1","spectra","스펙트라"));
        String blogId2 = blogService.createBlog(new BlogCdo("블로그이름2","블로그설명2","gdhong","홍길동"));

        String categoryId1 = categoryService.registerCategory(new CategoryCdo("블로그1-카테고리1", blogId1));
        String categoryId2 = categoryService.registerCategory(new CategoryCdo("블로그1-카테고리2", blogId1));
        String categoryId3 = categoryService.registerCategory(new CategoryCdo("블로그2-카테고리1", blogId2));

        postService.registerPost(new PostCdo("spectra", categoryId1, "블로그1-제목1", "내용1", Arrays.asList("태그1","태그2")));
        postService.registerPost(new PostCdo("spectra", categoryId1, "블로그1-제목2", "내용2", Arrays.asList("태그3","태그4")));
        postService.registerPost(new PostCdo("spectra", categoryId1, "블로그1-제목3", "내용3", Arrays.asList("태그3","태그4")));
        postService.registerPost(new PostCdo("spectra", categoryId2, "블로그1-제목4", "내용4", Arrays.asList("태그4","태그5")));
        postService.registerPost(new PostCdo("spectra", categoryId2, "블로그1-제목5", "내용5", Arrays.asList("태그6","태그7")));
        postService.registerPost(new PostCdo("gdhong", categoryId3, "블로그2-제목6", "내용6", Arrays.asList("태그1","태그2")));
    }
}
