package spectra.board.jpa;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.annotation.DirtiesContext;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import spectra.board.domain.entity.Blog;

@RunWith(SpringJUnit4ClassRunner.class)
@SpringBootTest(classes = BoardStoreTestApplication.class)
@DirtiesContext(classMode= DirtiesContext.ClassMode.AFTER_EACH_TEST_METHOD)
public class BlogJpaStoreTest {
    //
    @Autowired
    private BlogJpaStore blogStore;

    @Before
    public void setUp() {
        // create blog
        Blog blog = new Blog("free 블로그", "free", "프리");
        blog.setDescription("free 블로그 입니다.");
        String blogId = blogStore.create(blog);
        System.out.println("[TEST INFO] blog create => ID : " + blogId);
    }

    @Test
    public void testCreate() {
        // create blog
        Blog blog = new Blog("bird 블로그", "bird", "버드");
        blog.setDescription("bird 블로그 입니다.");
        String blogId = blogStore.create(blog);
        System.out.println("[TEST INFO] blog create => ID : " + blogId);
    }

    @Test
    public void testRetrieve() {
        // retrieve blog
        Blog blog = blogStore.retrieveByUserId("free");
        System.out.println("[TEST INFO] blog retrieve => blog : " + blog);
        System.out.println("[TEST INFO] blog retrieve => ID : " + blog.getId());
        System.out.println("[TEST INFO] blog retrieve => Desc : " + blog.getDescription());
    }
}
