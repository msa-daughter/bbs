package spectra.board.jpa.springdata;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.data.repository.PagingAndSortingRepository;
import spectra.board.jpa.jpo.PostJpo;

import java.util.List;

public interface PostRepository extends PagingAndSortingRepository<PostJpo, String> {
    Page<PostJpo> findAll(Specification<PostJpo> spec, Pageable pageable);
    List<PostJpo> findByBlogId(String blogId, Pageable pageable);
    List<PostJpo> findByCategoryId(String categoryId, Pageable pageable);
    List<PostJpo> findByContent(String content, Pageable pageable);
    List<PostJpo> findByTitle(String title, Pageable pageable);
    List<PostJpo> findByTitleAndContent(String title, String content,  Pageable pageable);
}
