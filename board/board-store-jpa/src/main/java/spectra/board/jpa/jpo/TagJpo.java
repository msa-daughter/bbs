package spectra.board.jpa.jpo;

import spectra.board.domain.entity.Tag;
import spectra.share.util.json.JsonUtil;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import java.util.*;
import java.util.stream.Collectors;

@Entity(name = "T_TAG")
public class TagJpo {
	//
	@Id
	private String id;

	@Column(unique = true)
	private String tagName;
	private int useCount;
	private String createdDate;

	private String blogId;
	private String postIds;


	public TagJpo() {
	    //
	}

	public static TagJpo toJpo(Tag tag){
	    TagJpo tagJpo = new TagJpo();
	    tagJpo.setId(tag.getId());
        tagJpo.setTagName(tag.getTagName());
        tagJpo.setUseCount(tag.getUseCount());
        tagJpo.setCreatedDate(tag.getCreatedDate());
        tagJpo.setBlogId(tag.getBlogId());
		tagJpo.setPostIds(tag.getPostIds());
        return tagJpo;
    }

    public static List<TagJpo> toJpos(List<Tag> tags){
	    List<TagJpo> tagJpos = new ArrayList<>();

        for(Tag tag:tags){
            tagJpos.add(TagJpo.toJpo(tag));
        }
        return tagJpos;
    }

    public Tag toDomain(){
        Tag tag = new Tag(id);
        tag.setTagName(tagName);
        tag.setUseCount(useCount);
        tag.setCreatedDate(createdDate);
        tag.setBlogId(blogId);
		tag.setPostIds(getPostIds());
        return tag;
    }

	public static List<Tag> toDomains(List<TagJpo> tagJpos){
		return tagJpos.stream()
				.map(jpos->jpos.toDomain())
				.collect(Collectors.toList());
	}

	public static Set<Tag> toDomains(Set<TagJpo> tagJpos){
		Set<Tag> tags = new HashSet<>();
		for (TagJpo jpos:tagJpos){
			Tag tag = new Tag();
			tag = jpos.toDomain();
			tags.add(tag);
		}
		return tags;
	}


    @Override
	public boolean equals(Object o) {
		if (this == o) return true;
		if (o == null || getClass() != o.getClass()) return false;
		TagJpo tagJpo = (TagJpo) o;
		return Objects.equals(tagName, tagJpo.tagName);
	}

	@Override
	public int hashCode() {
		return Objects.hash(tagName);
	}

    //getter and setter

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getTagName() {
		return tagName;
	}

	public void setTagName(String tagName) {
		this.tagName = tagName;
	}

	public int getUseCount() {
		return useCount;
	}

	public void setUseCount(int useCount) {
		this.useCount = useCount;
	}

	public String getCreatedDate() {
		return createdDate;
	}

	public void setCreatedDate(String createdDate) {
		this.createdDate = createdDate;
	}

	public String getBlogId() {
		return blogId;
	}

	public void setBlogId(String blogId) {
		this.blogId = blogId;
	}

	public List<String> getPostIds() {
		return JsonUtil.fromJson(postIds, List.class);
	}

	public void setPostIds(List<String> postIds) {
		this.postIds = JsonUtil.toJson(postIds);
	}
}
