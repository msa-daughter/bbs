package spectra.board.jpa;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import spectra.board.domain.entity.Blog;
import spectra.board.domain.store.BlogStore;
import spectra.board.jpa.jpo.BlogJpo;
import spectra.board.jpa.springdata.BlogRepository;

import java.util.NoSuchElementException;

@Repository
public class BlogJpaStore implements BlogStore {
    //
    @Autowired
    private BlogRepository blogRepository;

    @Override
    public String create(Blog blog) {
        BlogJpo blogJpo = BlogJpo.toJpo(blog);
        blogRepository.save(blogJpo);
        return blog.getId();
    }

    @Override
    public Blog retrieve(String id) throws NoSuchElementException {
        BlogJpo blogJpo =  blogRepository.findOne(id);
        if (blogJpo == null)
            throw new NoSuchElementException(String.format("No blog jpo[ID:%s] to retrieve.", id));

        return blogJpo.toDomain();
    }

    @Override
    public Blog retrieveByUserId(String userId) {
        BlogJpo blogJpo =  blogRepository.findByUserId(userId);
        return blogJpo == null ? null : blogJpo.toDomain();
    }

    @Override
    public void update(Blog blog) {
        BlogJpo blogJpo = BlogJpo.toJpo(blog);
        blogRepository.save(blogJpo);
    }

    @Override
    public void delete(String blogId) {
        blogRepository.delete(blogId);
    }

    @Override
    public void delete(Blog blog) {
        delete(blog.getId());
    }
}
