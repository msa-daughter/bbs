package spectra.board.jpa.springdata;

import org.springframework.data.repository.PagingAndSortingRepository;
import spectra.board.jpa.jpo.CommentJpo;

import java.util.List;

public interface CommentRepository extends PagingAndSortingRepository<CommentJpo, String> {
    List<CommentJpo> findByPostId(String postId);
}
