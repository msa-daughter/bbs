package spectra.board.jpa.springdata;

import org.springframework.data.repository.PagingAndSortingRepository;
import spectra.board.jpa.jpo.BlogJpo;

public interface BlogRepository extends PagingAndSortingRepository<BlogJpo, String> {
    BlogJpo findByUserId(String userId);
}
