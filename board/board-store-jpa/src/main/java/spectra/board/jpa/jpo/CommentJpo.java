package spectra.board.jpa.jpo;

import spectra.board.domain.entity.Comment;

import javax.persistence.Entity;
import javax.persistence.Id;
import java.util.List;
import java.util.stream.Collectors;

@Entity(name = "T_COMMENT")
public class CommentJpo {
    //
    @Id
    private String id;

    private String content;
    private String createdDate;

    private String postId;
    private String userId;
    private String userName;

    private Long entityVersion;

    public CommentJpo() {
        //
    }

    public static CommentJpo toJpo(Comment comment) {
        CommentJpo blogJpo = new CommentJpo();
        blogJpo.setId(comment.getId());
        blogJpo.setContent(comment.getContent());
        blogJpo.setCreatedDate(comment.getCreatedDate());
        blogJpo.setPostId(comment.getPostId());
        blogJpo.setUserId(comment.getUserId());
        blogJpo.setUserName(comment.getUserName());
        blogJpo.setEntityVersion(comment.getEntityVersion());
        return blogJpo;
    }

    public Comment toDomain() {
        Comment comment = new Comment(id);
        comment.setContent(content);
        comment.setCreatedDate(createdDate);
        comment.setPostId(postId);
        comment.setUserId(userId);
        comment.setUserName(userName);
        comment.setEntityVersion(entityVersion);
        return comment;
    }

    public static List<Comment> toDomains(List<CommentJpo> commentJpos){
        return commentJpos.stream()
                .map(jpo->jpo.toDomain())
                .collect(Collectors.toList());
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }

    public String getCreatedDate() {
        return createdDate;
    }

    public void setCreatedDate(String createdDate) {
        this.createdDate = createdDate;
    }

    public String getPostId() {
        return postId;
    }

    public void setPostId(String postId) {
        this.postId = postId;
    }

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public Long getEntityVersion() {
        return entityVersion;
    }

    public void setEntityVersion(Long entityVersion) {
        this.entityVersion = entityVersion;
    }
}
