package spectra.board.jpa.jpo;

import spectra.board.domain.entity.Greet;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import java.util.List;
import java.util.stream.Collectors;

@Entity
@Table(name = "T_GREET")
public class GreetJpo {
    @Id
    private String id;

    private String message;

    public GreetJpo(){
        //
    }

    public GreetJpo(String id, String message){
        this.id = id;
        this.message = message;
    }


    public static GreetJpo toJpo(Greet greet){
        GreetJpo greetJpo = new GreetJpo();
        greetJpo.setId(greet.getId());
        greetJpo.setMessage(greet.getMessage());
        return greetJpo;
    }

    public Greet toDomain(){
        Greet greet = new Greet(id);
        greet.setMessage(message);
        return greet;
    }

    public static List<Greet> toDomains(List<GreetJpo> greetJpos){
        return greetJpos.stream()
                .map(jpo->jpo.toDomain())
                .collect(Collectors.toList());
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }
}
