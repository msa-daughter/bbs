package spectra.board.jpa.jpo;

import spectra.board.domain.entity.Post;
import spectra.share.util.json.JsonUtil;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

@Entity(name="T_POST")
public class PostJpo {
    //
    @Id
    private String id;

    @Column(nullable = false)
    private String title;
    @Lob
    private String content;
    private String createdDate;
    private String updatedDate;

    private String blogId;
    private String categoryId;

    private String tagNames;

    public PostJpo(){
        //
    }

    public static PostJpo toJpo(Post post){
        PostJpo postJpo = new PostJpo();
        postJpo.setId(post.getId());
        postJpo.setTitle(post.getTitle());
        postJpo.setContent(post.getContent());
        postJpo.setCreatedDate(post.getCreatedDate());
        postJpo.setUpdatedDate(post.getUpdatedDate());
        postJpo.setBlogId(post.getBlogId());
        postJpo.setCategoryId(post.getCategoryId());
        postJpo.setTagNames(post.getTagNames());
        return postJpo;
    }

    public static List<PostJpo> toJpos(List<Post> posts){
        List<PostJpo> postJpos = new ArrayList<>();

        for(Post post:posts){
            postJpos.add(PostJpo.toJpo(post));
        }
        return postJpos;
    }

    public Post toDomain(){
        Post post = new Post(id);
        post.setTitle(title);
        post.setContent(content);
        post.setCreatedDate(createdDate);
        post.setUpdatedDate(updatedDate);
        post.setBlogId(blogId);
        post.setCategoryId(categoryId);
        post.setTagNames(JsonUtil.fromJson(tagNames, List.class));
        return post;

    }

    public static List<Post> toDomains(List<PostJpo> postJpos){
        return postJpos.stream()
                .map(jpos->jpos.toDomain())
                .collect(Collectors.toList());
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof PostJpo)) return false;
        return id != null && id.equals(((PostJpo) o).id);
    }

    @Override
    public int hashCode() {
        return 31;
    }

    //getter and setter

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }

    public String getCreatedDate() {
        return createdDate;
    }

    public void setCreatedDate(String createdDate) {
        this.createdDate = createdDate;
    }

    public String getUpdatedDate() {
        return updatedDate;
    }

    public void setUpdatedDate(String updatedDate) {
        this.updatedDate = updatedDate;
    }

    public String getBlogId() {
        return blogId;
    }

    public void setBlogId(String blogId) {
        this.blogId = blogId;
    }

    public String getCategoryId() {
        return categoryId;
    }

    public void setCategoryId(String categoryId) {
        this.categoryId = categoryId;
    }

    public List<String> getTagNames() {
        return JsonUtil.fromJson(tagNames, List.class);
    }

    public void setTagNames(List<String> tagNames) {
        this.tagNames = JsonUtil.toJson(tagNames);
    }
}
