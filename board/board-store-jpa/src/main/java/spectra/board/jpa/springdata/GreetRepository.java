package spectra.board.jpa.springdata;

import org.springframework.data.repository.PagingAndSortingRepository;
import spectra.board.jpa.jpo.GreetJpo;

public interface GreetRepository extends PagingAndSortingRepository<GreetJpo, String>{
    GreetJpo findById(String id);
}
