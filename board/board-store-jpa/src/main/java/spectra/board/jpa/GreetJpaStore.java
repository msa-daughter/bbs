package spectra.board.jpa;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import spectra.board.domain.entity.Greet;
import spectra.board.domain.store.GreetStore;
import spectra.board.jpa.jpo.GreetJpo;
import spectra.board.jpa.springdata.GreetRepository;

import java.util.NoSuchElementException;

@Repository
public class GreetJpaStore implements GreetStore{

    @Autowired
    private GreetRepository greetRepository;

    @Override
    public String create(Greet greet) {
        GreetJpo greetJpo = GreetJpo.toJpo(greet);
        greetRepository.save(greetJpo);
        return greet.getId();
    }

    @Override
    public Greet retrieve(String id) throws NoSuchElementException {
        GreetJpo greetJpo = greetRepository.findById(id);

        if (greetJpo == null) {
            throw new NoSuchElementException(String.format("No greet[%s] to retrieve.", id));
        }
        return greetJpo.toDomain();
    }

    @Override
    public void update(Greet greet) {
        GreetJpo greetJpo = GreetJpo.toJpo(greet);
        greetRepository.save(greetJpo);
    }

    @Override
    public void delete(Greet greet) {
        delete(greet.getId());
    }

    @Override
    public void delete(String id) {
        greetRepository.delete(id);
    }
}
