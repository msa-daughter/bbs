package spectra.board.jpa.springdata;

import org.springframework.data.repository.PagingAndSortingRepository;
import spectra.board.jpa.jpo.CategoryJpo;

import java.util.List;

public interface CategoryRepository extends PagingAndSortingRepository<CategoryJpo, String> {
    CategoryJpo findById(String id);
    List<CategoryJpo> findByBlogId(String blogId);
}
