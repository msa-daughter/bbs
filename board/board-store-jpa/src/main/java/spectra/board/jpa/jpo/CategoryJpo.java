package spectra.board.jpa.jpo;

import spectra.board.domain.entity.Category;

import javax.persistence.Entity;
import javax.persistence.Id;
import java.util.List;
import java.util.stream.Collectors;

@Entity(name = "T_CATEGORY")
public class CategoryJpo {
    @Id
    private String id;

    private String categoryName;
    private int useCount;
    private String createdDate;
    private String blogId;

    public CategoryJpo() {
        //
    }

    public static CategoryJpo toJpo(Category category)
    {
        CategoryJpo categoryJpo = new CategoryJpo();
        categoryJpo.setId(category.getId());
        categoryJpo.setCategoryName(category.getCategoryName());
        categoryJpo.setUseCount(category.getUseCount());
        categoryJpo.setCreatedDate(category.getCreatedDate());
        categoryJpo.setBlogId(category.getBlogId());
        return categoryJpo;
    }

    public Category toDomain()
    {
        Category category = new Category(id);
        category.setCategoryName(categoryName);
        category.setUseCount(useCount);
        category.setCreatedDate(createdDate);
        category.setBlogId(blogId);
        return category;
    }

    public static List<Category> toDomains(List<CategoryJpo> categoryJpo){
        return categoryJpo.stream()
                .map(jpos->jpos.toDomain())
                .collect(Collectors.toList());
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getCategoryName() {
        return categoryName;
    }

    public void setCategoryName(String categoryName) {
        this.categoryName = categoryName;
    }

    public int getUseCount() {
        return useCount;
    }

    public void setUseCount(int useCount) {
        this.useCount = useCount;
    }

    public String getCreatedDate() {
        return createdDate;
    }

    public void setCreatedDate(String createdDate) {
        this.createdDate = createdDate;
    }

    public String getBlogId() {
        return blogId;
    }

    public void setBlogId(String blogId) {
        this.blogId = blogId;
    }

}
